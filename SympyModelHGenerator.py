from sympy import *
from h_models_sympy import *
S1=0.5
S2=0.5
basis = basis_2center(S1,S2)
pprint(basis)
pprint(calc_S0_2center(basis))
pprint(calc_Spm_2center(basis))
J = symbols("J")
HHeisenberg = calc_JSaSb(J,basis)
pprint(HHeisenberg)
Dxx, Dyy, Dzz, Dxy, Dxz, Dyz = symbols("Dxx Dyy Dzz Dxy Dxz Dyz")
TensorD = zeros(3,3) 
TensorD[0,0] = Dxx
TensorD[1,1] = Dyy
TensorD[2,2] = Dzz
TensorD[0,1] = TensorD[1,0] = Dxy
TensorD[0,2] = TensorD[2,0] = Dxz
TensorD[1,2] = TensorD[2,1] = Dyz
TensorD[1,2] = TensorD[2,1] = Dyz
pprint(TensorD)

HAniso = calc_SaDSb(TensorD, basis)
pprint(HAniso)

dx, dy, dz = symbols("dx dy dz")
dab = zeros(1,3) 
dab[0] = dx
dab[1] = dy
dab[2] = dz
pprint(dab)

Hdab = calc_dab_SaxSb(dab, basis)
pprint(Hdab)
print(" ")
print("Coupled BASIS")
print(" ")
basis_coupled = coupled_basis_2center(S1,S2)
pprint(basis_coupled)

U = Clebsch_Gordan_uncoupled_to_coupled(basis,basis_coupled)
print('Matrice de Passage CG')
pprint(U)
Uinv = U.inv()
print('Matrice de Passage CG Inverse')
pprint(Uinv)

Hmodel_coupled_D0 =  HAniso * U 
Hmodel_coupled_D = expand(Uinv *  Hmodel_coupled_D0) 
print('Matrice anisotropie Dab')
pprint(Hmodel_coupled_D)


Hmodel_coupled_d0 =  Hdab * U 
Hmodel_coupled_d = expand(Uinv *  Hmodel_coupled_d0) 
print('Matrice anisotropie dab')
pprint(Hmodel_coupled_d)

Hmodel_coupled_J0 = HHeisenberg * U
Hmodel_coupled_J  = expand(Uinv * Hmodel_coupled_J0)
print('Matrice J')
pprint(Hmodel_coupled_J)

print('Matrice tot')
HtotCoupled = Hmodel_coupled_J+Hmodel_coupled_D+Hmodel_coupled_d
pprint(HtotCoupled)
#P, D = HtotCoupled.diagonalize()
#pprint(nsimplify(D[0,0]))
#pprint(D[0,0])
#Eigval = HtotCoupled.eigenvals()[0,0] 
#pprint(HtotCoupled.eigenvals()[0,0])

sys.exit()

