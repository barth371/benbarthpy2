from math import * 
import numpy as np
#TRUC RELOU POUR IMPORTER FONCTION DE REPERTOIRE AU DESSUS
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import subprocess
from subprocess import call
from functions import *
np.set_printoptions(precision=10,suppress=True)

#################################################
# Molcas Parser
#################################################
def parser_molcas_sym(outputname):

    stringE = 'STATE ENERGY'

    # setting indexes to 0
    n_state = 0 #Number of extraction of spin orbit energies
    index =0
    #dimensioning arrays
    file1 = open(outputname, "r")
    for line in file1: 
      index += 1
      if stringE in line:
        temp = line.split()
        n_state = len(temp)-2
        Line_loc = index +1
    file1.close() 
    # Number of CSF in the Molcas output
    with open(outputname,"r") as f:
      data = []
      data = f.readlines()
      nbreline = PositionFirstNum(data[Line_loc])

      nCSF = 0 
      while len(data[Line_loc+nCSF].strip()) != 0 : 
          nCSF+=1
    file1.close() 
    
    eigenvalues=np.zeros((nCSF, n_state))
    with open(outputname,"r") as f:
      data = []
      data = f.readlines()
      nbreline = PositionFirstNum(data[Line_loc])
      
      DetMolcas = []
      for i in range(0,nCSF):
          temp = data[Line_loc+i].split()
          #Recuperation du det en format Molcas
          dettemp =data[Line_loc+i].split("=")
          dettemp2=dettemp[0].split(")")
          DetMolcas.append(dettemp2[1].rstrip().lstrip())

          for j in range(0,n_state):
            eigenvalues[i,j]=temp[nbreline+j]
            
    return eigenvalues,DetMolcas,n_state,nCSF

def PositionFirstNum(E_line): #compute when the line is split where the = is and return the position of the first number
    temp = E_line.split()
    indice=0
    strloc=temp[indice]
    while strloc !=  "=" :
        indice+=1
        strloc=temp[indice]
    return indice+1


#################################################
# CASDI Parser
#################################################
#Output cleaner with line in 2 parts
def output_casdiModi(outputname):
# Check if we need to clean the casdi output:
    file1 = open(outputname, "r")
    checkcount = 0
    for line in file1: 
      if len(line) > 80 :
        checkcount += 1 
    file1.close() 
   #print(checkcount)
    file1 = open(outputname, "r")
    fileO = open('{0}.mod'.format(outputname), 'w')
    if checkcount == 0:
      for line in file1: 
        fileO.write(line)
    else:
      for line in file1: 
        if len(line) > 80 :
          temp = line.splitlines() 
        if len(line) < 80 :
          temp2 = temp + line.splitlines()
          finalLine= ''.join(temp2)
          fileO.write(finalLine+"\n")
    fileO.close() 
    file1.close() 
     
    return 
#Computing if Ndet CASDI == nCSF Molcas
def computing_Ndet_Casdi(outputname,n_state):
  string1 = 'ic='
  file1 = open(outputname, "r")
  nbreLine = 0
  for line in file1: 
    if string1 in line:
      nbreLine +=1
  Ndet = int(nbreLine/n_state)
  file1.close() 
  return Ndet

def parser_Casdi(outputname,nCSF,n_state,nOrbSym,NumOrb):
  string1 = 'ic='
  file1 = open(outputname, "r")
  lengthTab=nCSF*(n_state+1)
  
  coef = np.zeros(lengthTab)
  det = []
  cte = 0
  for line in file1: 
    if string1 in line:
      temp = line.split("=")
      detloc = convertStringMol(temp[1],nOrbSym,NumOrb)
      coefloc = temp[2].split(" ")[0]
     #print(detloc,coefloc)
      det.append(detloc)
      coef[cte] = coefloc
      #print(det[cte],coef[cte])
      cte +=1
 #print(det)
 #print(coef)
  eigenvaluesCasdi,detlistCasdi = putResultInTab(det,coef,nCSF,n_state)
  #print("nCSF,N_vec  =",nCSF,n_state,cte)
  # for i in range(0,nCSF):
  #   print(detlist[i],tablo[i,:])
  return eigenvaluesCasdi,detlistCasdi

def fromDetToCSF(detlistCasdi,eigenvaluesCasdi,nCSF,nDet,n_state):
  CSFCasdi = []
  EigenvaluesCSFCasdi = np.zeros((nCSF,n_state+1))
  cte= 2.0/sqrt(2.0)
  stringRemove = 'du' 
  stringMultiply = 'ud' 
  #print(orderedEigenvaluesCasdi )
  #print(orderedDetListCasdi)
  count = 0
  if nCSF == nDet/2:
    #print('Bolosss')
    for i in range(0,nDet):
      if i%2 ==1:
       #print("impaire ") 
       #print(detlistCasdi[i])
        CSFCasdi.append(detlistCasdi[i])
        EigenvaluesCSFCasdi[count:] = eigenvaluesCasdi[i]*cte
        count+=1 
  else:
    for i in range(0,nDet):
     #print(detlistCasdi[i])
      if stringRemove in detlistCasdi[i]:
        pass
        #print(" ") 
      elif stringMultiply in detlistCasdi[i]:
        CSFCasdi.append(detlistCasdi[i])
        EigenvaluesCSFCasdi[count:] = eigenvaluesCasdi[i]*cte
        count +=1
      else:
        CSFCasdi.append(detlistCasdi[i])
        EigenvaluesCSFCasdi[count:] = eigenvaluesCasdi[i]
        count +=1

 #print(CSFCasdi)
 #print(EigenvaluesCSFCasdi)
  return CSFCasdi,EigenvaluesCSFCasdi
 
def CasdiVectorOrderer(detlistCasdi,detlistMolcas,eigenvaluesCasdi,nCSF,n_state):
  orderedDetListCasdi = []
  orderedEigenvaluesCasdi = np.zeros((nCSF,n_state+1))
  #print(orderedEigenvaluesCasdi )
  #print(orderedDetListCasdi)
  for i in range(0,nCSF):
    for j in range(0,nCSF):
      if detlistMolcas[i] == detlistCasdi[j]:
        orderedDetListCasdi.append(detlistCasdi[j])
        orderedEigenvaluesCasdi[i:] = eigenvaluesCasdi[j]
  #print(orderedEigenvaluesCasdi)
  return orderedDetListCasdi,orderedEigenvaluesCasdi
  

def putResultInTab(detVec,coefVec,nCSF,n_state):
  tabVec = np.zeros((nCSF,n_state+1))
  detlist = []
  itot = 0
  for j in range(0,nCSF):
    detlist.append(detVec[j])
  itot = 0
  for i in range(0,n_state+1):
    for j in range(0,nCSF):
      tabVec[j,i] =coefVec[itot]
      itot+=1
  return tabVec,detlist


def convertStringMol(stringChain,nOrbSym,NumOrb):
  dim_nOrbSym = len(nOrbSym)
  dim_NumOrb = len(NumOrb)
  
  upelec = np.zeros(dim_NumOrb)
  dnelec = np.zeros(dim_NumOrb)
  
  temp=stringChain.split(",")
  dimtemp= len(temp)-1
  temp = np.delete(temp, dimtemp)
  
  for i in range(0,dimtemp):
    # print(int(temp[i]))
    for j in range(0,len(NumOrb)):
      if int(temp[i]) == NumOrb[j]:
        upelec[j] +=1
      if int(temp[i]) == -NumOrb[j]:
        dnelec[j] +=1
  totelec=upelec+dnelec
 #print('upelec',upelec)
 #print('dnelec',dnelec)
  index =0 
  detString =""
  for i in range(0,dim_nOrbSym):
    if nOrbSym[i] > 0:
     detString = detString + " "
    j=0
    while j < nOrbSym[i]:
      if totelec[index] == 0:
       detString = detString + "0"
      if totelec[index] == 1:
        if upelec[index] == 1:
          detString = detString + "d"
        if dnelec[index] == 1:
          detString = detString + "u"
      if totelec[index] == 2:
        detString = detString + "2"
      index+=1
      j+=1
  detString = detString.lstrip()
  return detString


#################################################
# Molcas Printer
#################################################
  
def molcas_printer(outputname,orderedEigenvaluesCasdi,nCSF,n_state):
    stringE = 'STATE ENERGY'
    #dimensioning arrays
    index =0
    file1 = open(outputname, "r")
    for line in file1: 
      index += 1
      if stringE in line:
        temp = line.split()
        n_state = len(temp)-2
        Line_loc = index +1
    file1.close() 

    fileO = open('{0}.mod'.format(outputname), 'w')

    with open(outputname,"r") as f:
      data = []
      data = f.readlines()
      nbreline = PositionFirstNum(data[Line_loc])
      sizeFile0 = len(data)
      #print(sizeFile0)  

      i =0
      while i < sizeFile0:
        if i == Line_loc:
          for j in range(0,nCSF):
            originalLine = data[i]
            orderedDetLineCasdi = orderedEigenvaluesCasdi[j,:]
            newLine = molcas_printLine(originalLine,orderedDetLineCasdi,n_state)
            fileO.write(newLine)
            i+=1
        else:
          fileO.write(data[i])
          i+=1	

    fileO.close() 
    return 
  
def molcas_printLine(originalLine,orderedDetLineCasdi,n_state):
  temp=originalLine.split("=")
  temp2=''
  for i in range(0,n_state):
    if orderedDetLineCasdi[i] < 0:
      localNum = format(orderedDetLineCasdi[i], ".12f")
      temp2+="     " + localNum 
    if orderedDetLineCasdi[i] > 0:
      localNum = format(orderedDetLineCasdi[i], ".12f")
      temp2+="      " + localNum 
  finalString= temp[0] + "=" + temp2 + "\n"
  return finalString 


#################################################
# Main function
#################################################


def from_CASDI_to_molcas(output_molcas,output_casdi,output_casdi_casci,NumOrb,nOrbSym,ortho):
  output_casdi2 = '{0}.mod'.format(output_casdi)
  output_casdi2_casci = '{0}.mod'.format(output_casdi_casci)

  ###########################################
  #Parsing the Molcas File
  ###########################################
  Eigenvalue, DetMolcas,n_state, nCSF =parser_molcas_sym(output_molcas)
 #print(Eigenvalue)
  #print('molcas cSF',DetMolcas) 

  ###########################################
  #Parsing the CASDI ddci  
  ###########################################
  #Cleaning the CASDI OUTPUT if needed
  output_casdiModi(output_casdi)
  #Parsing the cleaned output
  Ndet = computing_Ndet_Casdi(output_casdi2,n_state)
  if Ndet == nCSF:
    eigenvaluesCasdi,detlistCasdi=parser_Casdi(output_casdi2,Ndet,n_state,nOrbSym,NumOrb)
  elif Ndet != nCSF:
    eigenvaluesCasdi0,detlistCasdi0=parser_Casdi(output_casdi2,Ndet,n_state,nOrbSym,NumOrb)
    detlistCasdi,eigenvaluesCasdi = fromDetToCSF(detlistCasdi0,eigenvaluesCasdi0,nCSF,Ndet,n_state)
    #print('plus gros',detlistCasdi0)
  else:
    sys.exit() 

  #eigenvaluesCasdi,detlistCasdi=parser_Casdi(output_casdi2,Ndet,n_state,nOrbSym,NumOrb)
  #print('amincie',detlistCasdi)
  #Ordering the CASDI WF according to the molcas one
  orderedDetListCasdi,orderedEigenvaluesCasdi = CasdiVectorOrderer(detlistCasdi,DetMolcas,eigenvaluesCasdi,nCSF,n_state)
  #print('Ordone',orderedDetListCasdi)
  #print(DetMolcas)
  #We truncate because casdi casci is VERY DUMB and print more states
  eigenvaluesCasdiTruncated = np.delete(orderedEigenvaluesCasdi, n_state, 1)
 #print(orderedEigenvaluesCasdi)
 #print(Eigenvalue)

  ###########################################
  #othronormalisation of CASDI DDCI vectors
  ###########################################
  if ortho == 'GS': 
    orthoCasdi = Gram_schmidt(eigenvaluesCasdiTruncated)
  elif ortho == 'DCloiseaux': 
    orthoCasdi = Des_cloizeaux(eigenvaluesCasdiTruncated)
  else:
    orthoCasdi = eigenvaluesCasdiTruncated

 #vectest= orthoCasdi
 #for i in range(0,6):
 # print('vector',i)
 # for j in range(0,6):
 #   vec1 = vectest[:, i].copy()
 #   vec2 = vectest[:, j].copy()
 #   dotProduct = np.dot(vec2,vec1)
 #   print(dotProduct)
 
  molcas_printer(output_molcas,orthoCasdi,nCSF,n_state)

  ###########################################
  #Parsing the CASDI CASCI File for verification Purpose
  ###########################################
  #Cleaning the CASDI OUTPUT
  output_casdiModi(output_casdi_casci)
  #Parsing the cleaned output
  if Ndet == nCSF:
    eigenvaluesCasdi_casci,detlistCasdi_casci=parser_Casdi(output_casdi2_casci,nCSF,n_state,nOrbSym,NumOrb)
  elif Ndet != nCSF:
    eigenvaluesCasdi0_casci,detlistCasdi0_casci=parser_Casdi(output_casdi2_casci,Ndet,n_state,nOrbSym,NumOrb)
    detlistCasdi_casci,eigenvaluesCasdi_casci = fromDetToCSF(detlistCasdi0_casci,eigenvaluesCasdi0_casci,nCSF,Ndet,n_state)


  #print(eigenvaluesCasdi_casci,detlistCasdi_casci)
  #Ordering the CASDI WF according to the molcas one
  orderedDetListCasdi_casci,orderedEigenvaluesCasdi_casci = CasdiVectorOrderer(detlistCasdi_casci,DetMolcas,eigenvaluesCasdi_casci,nCSF,n_state)
  #We truncate because casdi casci is VERY DUMB and print more states
  eigenvaluesCasdiTruncated_casci = np.delete(orderedEigenvaluesCasdi_casci, n_state, 1)
 
 
  error_file= '{0}.error_tracking_file'.format(output_molcas) 
  #Writing an error tracking file :
  with open(error_file, 'w') as f:
  #with open('filename.txt', 'w') as f:
      print('PARSING FROM MOLCAS',file=f)
      print('   ',file=f)
      print('NCSF =',nCSF,file=f)
      print('NdetCASDI =',Ndet,file=f)
      print('NState =',n_state,file=f)
      print('Eigenvalue MOLCAS  =',file=f)
      print(Eigenvalue,file=f)
      print('det Molcas= ',DetMolcas,file=f)
      print('   ',file=f)
      print('*****************************',file=f)
      print('PARSING FROM CASDI CASCI',file=f)
      if Ndet != nCSF:
        print('   ',file=f)
        print('WARNING : I did a not trivial Conversion from CASDI Determinant to CSF working only in the case of Cu2Cl5',file=f)
        print('det list CASDI =',file=f)
        print(detlistCasdi0,file=f)    
        print('CSF list CASDI =',file=f)
        print(detlistCasdi,file=f)    
        print('   ',file=f)
      print('   ',file=f)
      print('Eigenvalues CASDI CASCI', file=f)
      print(eigenvaluesCasdiTruncated_casci, file=f)
      print('   ',file=f)
      print('Eigenvalues CASDI CASCI - MOLCAS CASCI', file=f)
      print(eigenvaluesCasdiTruncated_casci - Eigenvalue, file=f)
      print('   ',file=f)
      print('Eigenvalues CASDI CASCI + MOLCAS CASCI', file=f)
      print(eigenvaluesCasdiTruncated_casci + Eigenvalue, file=f)
      print('*****************************',file=f)
      print('PARSING FROM CASDI ddci',file=f)
      print('   ',file=f)
      print('Eigenvalues CASDI ddci : ', file=f)
      print(eigenvaluesCasdiTruncated, file=f)
      print('   ',file=f)
      print('Orthonormalised CASDI Eigenvalues ddci : ', file=f)
      print('with',ortho,' method ', file=f)
      print(orthoCasdi, file=f)
      print('   ',file=f)
      print('Reminder eigenvalues MOLCAS : ', file=f)
      print(Eigenvalue, file=f)
      print('   ',file=f)


##################################
#SCRIPT FOR USING MOHAMED SCRIPT
####################################
#Program that change the 2nd line of the inp file
def changeLineTextFile(outputname,nLine,newLine):
  file1 = open(outputname, "r")
  fileTemp = open('temp', "w")
  count = 0
  for line in file1: 
    if count == nLine :
      fileTemp.write(newLine+"\n")
    else :
      fileTemp.write(line)
    count +=1
  fileTemp.close() 
  file1.close() 
  subprocess.run('mv {0} {1}'.format('temp',outputname), shell=True)
  return 


##################################
#PARSING RASSI OUTPUT JUST FOR Cu2Cl5R
####################################

def parser_Rassi_out(outputname):

    stringE = 'Energy (au)'

    # setting indexes to 0
    n_state = 4 #Number of extraction of spin orbit energies
    index =0
   #E_state =np.zeros((dim_H,dim_H),dtype=float)
    E_state =np.zeros((n_state),dtype=float)
    
    vectors_state = np.zeros((4,4),dtype=complex)
    #dimensioning arrays
    file1 = open(outputname, "r")
    for line in file1: 
      index += 1
      if stringE in line:
        temp = line.split()
       #print(temp)
        E_state[0] = temp[2]
        E_state[1] = temp[3]
        E_state[2] = temp[4]
        E_state[3] = temp[5]
        Line_loc = index  +1
       #print(E_state[0])
        break
    file1.close() 

    with open(outputname,"r") as f:
      data = []
      data = f.readlines()
      count = 0 
     #CAS(18,10) ORIGINAL
     #for i in [0, 1, 2, 75]: 
     #  test CAS(6,4)
      for i in [0, 1, 2, 12]: 
        nbreline = data[Line_loc+i]
       #print(Line_loc+i)
       #print(nbreline) 
        local_vec = convert_Rassi_line(nbreline)
        vectors_state[0,count] = local_vec[0]
        vectors_state[1,count] = local_vec[1]
        vectors_state[2,count] = local_vec[2]
        vectors_state[3,count] = local_vec[3]
        count +=1

    file1.close() 
   #print(vectors_state)
    return E_state, vectors_state

def convert_Rassi_line(line):
    #print(line)
    vectors = np.zeros((4),dtype=complex)
    
    temp = line.split()     
    
    for i in range(0,4):
      vecstate1 = temp[3+i].split(',')     
     
      vecreal = vecstate1[0][1:]
      vecreal = float(vecreal.replace("D", "E"))
     
      vecimg = vecstate1[1][:-1]
      vecimg = float(vecimg.replace("D", "E"))

      nbre = complex(vecreal,vecimg)
      vectors[i] = nbre

     #print(vectors)
    return vectors 


def parser_Rassi_out4S4T(outputname):

    stringE = 'Energy (au)'

    # setting indexes to 0
    n_state = 16 #Number of extraction of spin orbit energies
    index =0
    count_numberState = 0
   #E_state =np.zeros((dim_H,dim_H),dtype=float)
    E_state =np.zeros((n_state),dtype=float)
    LocStates = np.zeros((4),dtype=int) 
    vectors_state = np.zeros((16,16),dtype=complex)
    #dimensioning arrays
    file1 = open(outputname, "r")
    for line in file1: 
      index += 1
      if stringE in line and count_numberState < 4:
        temp = line.split()
       #print(temp)
       #print(count_numberState)
        E_state[count_numberState*4+0] = temp[2]
        E_state[count_numberState*4+1] = temp[3]
        E_state[count_numberState*4+2] = temp[4]
        E_state[count_numberState*4+3] = temp[5]
        Line_loc = index  +1
        LocStates[count_numberState] = Line_loc
        count_numberState += 1
         #print(E_state[0])
         #break
    file1.close() 
   #print(LocStates)
    count_numberState = 0
    with open(outputname,"r") as f:
      data = []
      data = f.readlines()
      while count_numberState < 4 : 
        Line_locTemp = LocStates[count_numberState]
        #CAS(18,10) ORIGINAL
        count = 0 
        #for i in [0,1,2,3,4,5,6,7,8,9,10,11,75,76,77,78]: 
       #for i in [2,5,8,11,1,4,7,10,0,3,6,9,75,76,77,78]: 
        #FOr CAS(6,4)
        for i in [2,5,8,11,1,4,7,10,0,3,6,9,12,13,14,15]: 
          nbreline = data[Line_locTemp+i]
         #print(nbreline) 
          local_vec = convert_Rassi_line(nbreline)
          vectors_state[count_numberState*4+0,count] = local_vec[0]
          vectors_state[count_numberState*4+1,count] = local_vec[1]
          vectors_state[count_numberState*4+2,count] = local_vec[2]
          vectors_state[count_numberState*4+3,count] = local_vec[3]
          count +=1

        count_numberState += 1

    file1.close() 
   #print(vectors_state)
    return E_state, vectors_state



def parser_Rassi_out4S4TSymField(outputname):

    stringE = 'Energy (au)'

    # setting indexes to 0
    n_state = 16 #Number of extraction of spin orbit energies
    index =0
    count_numberState = 0
   #E_state =np.zeros((dim_H,dim_H),dtype=float)
    E_state =np.zeros((n_state),dtype=float)
    LocStates = np.zeros((4),dtype=int) 
    vectors_state = np.zeros((16,16),dtype=complex)
    #dimensioning arrays
    file1 = open(outputname, "r")
    for line in file1: 
      index += 1
      if stringE in line and count_numberState < 4:
        temp = line.split()
       #print(temp)
       #print(count_numberState)
        E_state[count_numberState*4+0] = temp[2]
        E_state[count_numberState*4+1] = temp[3]
        E_state[count_numberState*4+2] = temp[4]
        E_state[count_numberState*4+3] = temp[5]
        Line_loc = index  +1
        LocStates[count_numberState] = Line_loc
        count_numberState += 1
         #print(E_state[0])
         #break
    file1.close() 
   #print(LocStates)
    count_numberState = 0
    with open(outputname,"r") as f:
      data = []
      data = f.readlines()
      while count_numberState < 4 : 
        Line_locTemp = LocStates[count_numberState]
        #CAS(18,10) ORIGINAL
        count = 0 
        #for i in [0,1,2,3,4,5,6,7,8,9,10,11,75,76,77,78]: 
       #for i in [2,5,8,11,1,4,7,10,0,3,6,9,75,76,77,78]: 
        #FOr CAS(6,4)
        for i in [2,5,11,8,1,4,10,7,0,3,9,6,12,15,13,14]: 
          nbreline = data[Line_locTemp+i]
         #print(nbreline) 
          local_vec = convert_Rassi_line(nbreline)
          vectors_state[count_numberState*4+0,count] = local_vec[0]
          vectors_state[count_numberState*4+1,count] = local_vec[1]
          vectors_state[count_numberState*4+2,count] = local_vec[2]
          vectors_state[count_numberState*4+3,count] = local_vec[3]
          count +=1

        count_numberState += 1

    file1.close() 
   #print(vectors_state)
    return E_state, vectors_state


###Gram schimtd S-12 tests
# Test_vectors=eigenvaluesCasdiTruncated
# print('Test_vectors')
# print(Test_vectors)
# print(' ')
# GRAM = Gram_schmidt(Test_vectors)
# print('GRAM')
# print(GRAM)
# print('GRAM-Test_vectors')
# print(GRAM-Test_vectors)
# print(' ')
# SM12 = Des_cloizeaux(Test_vectors)
# print('SM12')
# print(SM12)
#
# print('SM12 - GRAM')
# print(SM12 - GRAM)
#
# print('Ortho Gram vect 1')
#
# for i in range(0,6):
#  print('vector',i)
#  for j in range(0,6):
#    vec1 = SM12[:, i].copy()
#    vec2 = SM12[:, j].copy()
#    dotProduct = np.dot(vec2,vec1)
#    print(dotProduct)

