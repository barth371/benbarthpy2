from parser_molcas import *
#TRUC RELOU POUR IMPORTER FONCTION DE REPERTOIRE AU DESSUS
import os, sys
#currentdir = os.path.dirname(os.path.realpath(__file__))
#parentdir = os.path.dirname(currentdir)
#sys.path.append(parentdir)
#from functions import *
#from h_models import *
import numpy as np
#import matplotlib.pyplot as plt
#import sys
import subprocess
from subprocess import call
np.set_printoptions(precision=10,linewidth=200,suppress=True)


###########################################
#Global Input Cu2Cl5 CAS(18,10)
###########################################
#output_rassi ="outrassi.PsiDDCI.EDDCI_DCloiseaux_170"
output_rassi ="outrassi_OM_CAS18-10SCF"
output_rassi ="Output_Molcas_Field/outrassi.PsiCAS.ECAS.64"
output_rassi ="Output_Molcas_1810/outrassi.PsiCAS.ECAS_130"
output_rassi ="Output_Molcas_Field/nosym/outrassi_OM_CAS6-4SCF.nofield"
output_rassi ="Output_Molcas_Field/nosym/outrassi_OM_CAS6-4SCF.y01"
output_rassi ="Output_Molcas_Field/ourassiFieldComp/1810FieldSym"
output_rassi ="Output_Molcas_Field/SymNofield/outrassi.PsiCAS.ECAS"
output_rassi ="Output_Molcas_Field/Final/outputFielRassi/Nofield.PsiCAS.ECAS.64"
output_rassi ="Output_Molcas_Field/Final/outputFielRassi/Nofield.PsiDDCI.EDDCI.64"
output_rassi ="Output_Molcas_Field/Final/outputFielRassi/Field.PsiCAS.ECAS.64"
output_rassi ="Output_Molcas_Field/Final/outputFielRassi/Field.PsiDDCI.EDDCI.64"
#output_rassi ="Output_Molcas_1810/MolcasHeff.PsiCAS.ECAS_130"
#output_rassi ="MolcasHeff.PsiCAS.ECAS"
S2 = 1/2# second spin
S1 = 1/2# second spin
dim_Heff = 4 

###################################################################
# Parsing the Rassioutput
###################################################################
print('Parsing the outrassi file              ... ')
energies, vectors = parser_Rassi_out(output_rassi)
print('done')
print('Initial vectors :')
print(vectors)
print('Initial energies : ')
print(energies)
energies = energies - energies[0]
energies = energies*219474.6
print('Initial energies shifted in cm-1 : ')
print(energies)
###################################################################
# Building the effective Hamiltonian
###################################################################
print('Building the effective Hamiltonian  ',end='')
heff = heff_build(vectors,energies)
print('done')
print('Effective Hamiltonian :')
np.set_printoptions(precision=1,linewidth=200,suppress=True)
print(heff)
S_eigenval, S_eigenvec = np.linalg.eigh(heff)
print("Heff vector")
print(S_eigenvec)  
print("Heff E")
print(S_eigenval)  
#solver_1center(heff,spin)
#DA,DB,DAB,dAB = solver_2center(heff_uncoupled,S1,S2) # without rank 4 tensor

#sys.exit()

###################################################################
# Building the basis and matrices to go from coupled to uncoupled
###################################################################
print('Expressing H in uncoupled basis      ... ',end='')
basis_uncoupled = basis_2center(S1,S2)
basis_coupled = coupled_basis_2center(S1,S2)
U = Clebsch_Gordan_uncoupled_to_coupled(basis_uncoupled,basis_coupled)
Uinv = np.linalg.inv(U)
heff_uncoupled = np.dot(U,np.dot(heff,Uinv)) # express heff in uncoupled basis
print('done')
print('Effective Hamiltonian in coupled basis :')
print(heff)
print('Effective Hamiltonian in uncoupled basis :')
print(heff_uncoupled)

###################################################################
# Solving the least-square problem to get SH parameters
###################################################################
print('Extracting SH parameters             ... ',end='')
DA,DB,DAB,dAB = solver_2center(heff_uncoupled,S1,S2) # without rank 4 tensor
#DA,DB,DAB,dAB,DAB_rank4 = solver_2center_rank4(heff_uncoupled,S1,S2) # with rank 4 tensor
print('done')

###################################################################
# Building the model H and comparing with effective H
###################################################################
print('Computing errors                     ... ',end='')
H = H_2center(basis_uncoupled,DA,DB,DAB,dAB) # without rank 4 tensor
#H = H_2center_rank4(basis_uncoupled,DA,DB,DAB,dAB,DAB_rank4) # with rank 4 tensor
Herror_uncoupled = H-heff_uncoupled
Herror_coupled = np.dot(Uinv,np.dot(Herror_uncoupled,U))
mae, maxae, rmse = error_model(heff_uncoupled,H)
eigenvalue_model = np.linalg.eigvalsh(H)
mae_energy = np.mean(abs(eigenvalue_model-energies))
print('done')
print('   MAE(H)  = ', mae)
print('   MaxE(H) = ', maxae)
print('   MAE(E)  = ', mae_energy)

###################################################################
# Printing results
###################################################################
print('\n========================')
print('  Extracted Parameters')
print('========================\n')

DA_trace = np.trace(DA)
DA = DA - np.dot(DA_trace/3,np.identity((3)))
print('DA (Trace = ',DA_trace,')')
print(DA)
print('')

DB_trace = np.trace(DB)
DB = DB - np.dot(DB_trace/3,np.identity((3)))
print('DB (Trace = ',DB_trace,')')
print(DB)
print('')

DAB_trace = np.trace(DAB)
DAB = DAB - np.dot(DAB_trace/3,np.identity((3)))
print('J isotropic = ', DAB_trace/3,'\n')
print('DAB (Trace = ',DAB_trace,')')
print(DAB)
print('')

print('dAB')
print(dAB)
print('')
