from parser_molcas import *
#TRUC RELOU POUR IMPORTER FONCTION DE REPERTOIRE AU DESSUS
import os, sys
#currentdir = os.path.dirname(os.path.realpath(__file__))
#parentdir = os.path.dirname(currentdir)
#sys.path.append(parentdir)
#from functions import *
#from h_models import *
import numpy as np
#import matplotlib.pyplot as plt
#import sys
import subprocess
from subprocess import call
np.set_printoptions(precision=10,linewidth=200,suppress=True)


###########################################
#Global Input Cu2Cl5 CAS(18,10)
##########################################
CASDI_files =["out.ddcit2","out.ddcit1","out.ddcit3","out.ddcit4","out.ddcit5","out.ddcit6","out.ddcit7","out.ddcit8","out.ddcis1","out.ddcis2","out.ddcis3","out.ddcis4","out.ddcis5","out.ddcis6","out.ddcis7","out.ddcis8"]
NstatesSym=[6, 1, 3, 3, 3, 3, 3, 3, 6, 1, 3, 3, 3, 3, 3, 3 ]
Nfile = len(NstatesSym)
NstateTot = 0

for i in range(0,Nfile):
  NstateTot += NstatesSym[i]
print(NstateTot)

def get_rassi_E(outputname):
    
    stringE = 'VECTEUR PROPRE NUMERO'
    E_state = []
    # setting indexes to 0
    index =0
    #dimensioning arrays

    file1 = open(outputname, "r")
    for line in file1: 
      if stringE in line:
        temp = line.split()
        #print(temp)
        E_state.append(temp[7])
    file1.close() 
   #print(E_state)
    return E_state

line_to_print=[]
counttot= 0
Hext= 'Hext.Matrix'

with open(Hext, 'w') as f:
  for i in range(0,Nfile):
    E_state = get_rassi_E(CASDI_files[i])
    for nstate in range(0,NstatesSym[i]):
       locString =""
       for printstate in range(0,NstateTot):
           if printstate == counttot : 
             locString +=" " + E_state[nstate]
             print(E_state[nstate])
             break
           else:
             locString += " 0" 
       counttot +=1
       #print(locString)
       print(locString,file=f)
      
      #print("     ")
       line_to_print.append(locString)
print(counttot)
#print(line_to_print[0])



#Writing an error tracking file :

