from parser_molcas import *
#TRUC RELOU POUR IMPORTER FONCTION DE REPERTOIRE AU DESSUS
import os, sys
#currentdir = os.path.dirname(os.path.realpath(__file__))
#parentdir = os.path.dirname(currentdir)
#sys.path.append(parentdir)
#from functions import *
#from h_models import *
import numpy as np
#import matplotlib.pyplot as plt
#import sys
import subprocess
from subprocess import call
np.set_printoptions(precision=10,linewidth=200,suppress=True)

###########################################
#Global Input Cu2Cl5 CAS(6,4)
###########################################
#output_molcas = "JOBIPHOUT.S1.CASCI.ortho"
#output_casdi = "ESCASOUT.ddcis1.OMT.Loc"
#output_casdi_casci = "ESCASOUT.cascis1.OMT.Loc"
#NumOrb =  [33,34,35,36]
#nOrbSym = [2,0,2,0]
#ortho = 'GS'
#ortho = 'DCloiseaux'
#########################

###########################################
#Global Input Cu2Cl5 CAS(18,10)
###########################################
#output_molcas ="cu2.JobIphS.CAS18-10.sym1"
#output_casdi = "ESCASOUT.ddcis1"
#output_casdi_casci = "ESCASOUT.cascis1"
#output_molcas = "cu2.JOBIPHOUT.CAS18-10.sym2"
#output_casdi = "ESCASOUT.ddcit2"
#output_casdi_casci = "ESCASOUT.cascit2"
NumOrb =  [44,45,46,47,48,49,50,51,52,53]
nOrbSym = [2,2,1,1,1,1,1,1]
ortho = 'GS'
#ortho = 'DCloiseaux'
#########################

orthotype = ["GS","DCloiseaux"]
statesCASDI =["s1","s2","s3","s4","s5","s6","s7","s8","t1","t2","t3","t4","t5","t6","t7","t8"]
statesMolcas=["S.CAS18-10.sym1","S.CAS18-10.sym2","S.CAS18-10.sym3","S.CAS18-10.sym4","S.CAS18-10.sym5", "S.CAS18-10.sym6", "S.CAS18-10.sym7","S.CAS18-10.sym8","T.CAS18-10.sym1","T.CAS18-10.sym2","T.CAS18-10.sym3","T.CAS18-10.sym4","T.CAS18-10.sym5", "T.CAS18-10.sym6", "T.CAS18-10.sym7","T.CAS18-10.sym8"]

#statesCASDI =["s1","s2"]
#statesMolcas=["S.CAS18-10.sym1","S.CAS18-10.sym2"]

JobiphINP = "JOBIPH"
JobiphINP2 = "JOBIPHINP"
outoutMolcasMohame = "JOBIPHOUT"

for indice in range(len(orthotype)):
  ortho =orthotype[indice]
  for i in range(len(statesCASDI)): 
    output_casdi = "ESCASOUT.ddci{0}".format(statesCASDI[i])
    output_casdi_casci = "ESCASOUT.casci{0}".format(statesCASDI[i])
    localJobiph = "cu2.JobIph{0}".format(statesMolcas[i])
    print('Working with  :  ',output_casdi,output_casdi_casci,localJobiph,ortho) 
  
    #Runing first step of Mohamed script
    subprocess.run('cp {0} {1}'.format(localJobiph,JobiphINP), shell=True)
    changeLineTextFile("inp",1,'r')
    rc = call("./koko.sh")
    subprocess.run('cp {0} {0}.{1}'.format(outoutMolcasMohame,localJobiph), shell=True)
  
    #Taking CASDI WF and putting them in the JOBIPHOUT 
    from_CASDI_to_molcas(outoutMolcasMohame,output_casdi,output_casdi_casci,NumOrb,nOrbSym,ortho)
    subprocess.run('mv {0}.error_tracking_file {1}.error_tracking_file_{2}'.format(outoutMolcasMohame,localJobiph,ortho), shell=True)
  
    #Runing second part stet of Mohamed script
    subprocess.run('cp {0}.mod {1}'.format(outoutMolcasMohame,JobiphINP2), shell=True)
    changeLineTextFile("inp",1,'w')
    rc = call("./koko.sh")
  
    subprocess.run('mv {0} {1}.DDCI_{2}'.format(JobiphINP,localJobiph,ortho), shell=True)




