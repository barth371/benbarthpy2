import numpy as np
np.set_printoptions(precision=10,suppress=True)

#################################################
# Scan ORCA output and extract relevant vector/energies
# input : orca output
#         and a vector with 1st element the dimension of Heff,
#         2nd the Number of roots wanted of block 0, 
#         3rd number of root wanted in block 1,..... 
#         
# output : arrays containing vectors/energies
#          list of arrays in case of multiple extraction
#################################################
def parser_orca(outputname,N_extract):
    Nstates = N_extract[0]
    N_root = np.delete(N_extract, 0)
    dim_Nroot = len(N_root)

    #TEST VALIDITY OF INPUT 
    sum = 0
    for i in range(0,dim_Nroot):
      sum += N_root[i]

    if sum != Nstates:
      print('##########################')
      print('I hope you know what you are doing, Nstate is suposed to be equal to the sum of the number of root by block wanted!!!')
      print('##########################')
    #####################################

    stringE = 'Eigenvalues:     cm-1         eV      Boltzmann populations at T =  300.000 K'

    # setting indexes to 0
    n_extract_E = 0 #Number of extraction of spin orbit energies

    #dimensioning arrays
    file1 = open(outputname, "r")
    for line in file1: 
      if stringE in line:
        n_extract_E += 1
    file1.close() 

    Energie_line = np.zeros((n_extract_E, Nstates))
    extract_eigenvalues=np.zeros((n_extract_E, Nstates))
    extract_eigenvectors=np.zeros((n_extract_E, Nstates,Nstates,2))

    file1 = open(outputname, "r")
    index = 0
    n_extract_E = 0

    # Filling arrays 
    for line in file1:
        index += 1
        if stringE in line:
          n_extract_E += 1
        
        for istate in range(Nstates):
          if istate < 10 :
            stringV= "STATE   {}:".format(istate)
          else :
           stringV= "STATE  {}:".format(istate)
          if stringV in line:  
            Energie_line[n_extract_E-1,istate] = index
            
            
    for i in range(n_extract_E):
       
      for istate in range(Nstates):
        line_num=int(Energie_line[i,istate])
        extract_eigenvalues[i,istate] = extract_E(outputname, line_num)
        local_vector = extract_vector_component(outputname,line_num,Nstates,N_root)
        
        for ieigen in range(Nstates):

          extract_eigenvectors[i, istate,ieigen,0]=local_vector[ieigen][0]
          extract_eigenvectors[i, istate,ieigen,1]=local_vector[ieigen][1]

    #Vector changes:
    final_vector = []
    
    for nExt in range(n_extract_E):
      vectors_init = np.zeros((Nstates,Nstates),dtype=complex)
      for i in range(Nstates):
        for j in range(Nstates):
          nbre = complex(extract_eigenvectors[nExt ,j][i][0],extract_eigenvectors[nExt ,j][i][1])
          vectors_init[j,i] = nbre

      final_vector.append(vectors_init)

    return (extract_eigenvalues, final_vector)

def extract_real_img_parts(vector_line):
  temp = []
  temp = vector_line.split()
  real_img = [float(temp[1]),float(temp[2])]
  return real_img

def extract_vector_component(fileName, lineNumber ,NVect, N_root):
  #Line number of the vector decomposition in input and return the wanted components
  dim_Nroot = len(N_root)
  with open(fileName,"r") as f:
   data = []
   data = f.readlines()
   local_index = lineNumber #+1
   eigencomponent = []
   i = 0
   Nroot_loc = 0
   
   while i < NVect :
     string_temp = data[local_index]
     string_splited = string_temp.split()
     
     for blockindex in range(0,dim_Nroot):
      if (int(string_splited[4]) == blockindex and int(string_splited[5]) == 0 and Nroot_loc < N_root[blockindex] ):
        eigencomponent.append(extract_real_img_parts(string_temp))
        i += 1
        Nroot_loc += 1
      Nroot_loc = 0
      
     local_index +=1
   return eigencomponent

def extract_E_cm(E_line): #extract the cm-1 value
  temp = []
  temp = E_line.split()
  E_cm = float(temp[2])
  return E_cm

def extract_E(fileName, lineNumber):
  with open(fileName,"r") as f:
   data = []
   data = f.readlines()
   string_E = data[lineNumber-1]
   Estate = extract_E_cm(string_E)
   return Estate

####### PARSER WITH EXCITED STATE BY BLOCK


def parser_orca_excited(outputname,N_extract):
    Nstates = N_extract[0]
    N_root = np.delete(N_extract, 0)
    dim_Nroot = len(N_root)

    #TEST VALIDITY OF INPUT 
    sum = 0
    for i in range(0,dim_Nroot):
      sum += N_root[i]

    if sum != Nstates:
      print('##########################')
      print('I hope you know what you are doing, Nstate is suposed to be equal to the sum of the number of root by block wanted!!!')
      print('##########################')
    #####################################

    stringE = 'Eigenvalues:     cm-1         eV      Boltzmann populations at T =  300.000 K'

    # setting indexes to 0
    n_extract_E = 0 #Number of extraction of spin orbit energies

    #dimensioning arrays
    file1 = open(outputname, "r")
    for line in file1: 
      if stringE in line:
        n_extract_E += 1
    file1.close() 

    Energie_line = np.zeros((n_extract_E, Nstates))
    extract_eigenvalues=np.zeros((n_extract_E, Nstates))
    extract_eigenvectors=np.zeros((n_extract_E, Nstates,Nstates,2))

    file1 = open(outputname, "r")
    index = 0
    n_extract_E = 0

    # Filling arrays 
    for line in file1:
        index += 1
        if stringE in line:
          n_extract_E += 1
        
        for istate in range(Nstates):
          if istate < 10 :
            stringV= "STATE   {}:".format(istate)
          else :
            stringV= "STATE  {}:".format(istate)
          if stringV in line:  
            Energie_line[n_extract_E-1,istate] = index
            
            
    for i in range(n_extract_E):
       
      for istate in range(Nstates):
        line_num=int(Energie_line[i,istate])
        extract_eigenvalues[i,istate] = extract_E(outputname, line_num)
        local_vector = extract_vector_component_excited(outputname,line_num,Nstates,N_root)
        
        for ieigen in range(Nstates):

          extract_eigenvectors[i, istate,ieigen,0]=local_vector[ieigen][0]
          extract_eigenvectors[i, istate,ieigen,1]=local_vector[ieigen][1]

    #Vector changes:
    final_vector = []
    
    for nExt in range(n_extract_E):
      vectors_init = np.zeros((Nstates,Nstates),dtype=complex)
      for i in range(Nstates):
        for j in range(Nstates):
          nbre = complex(extract_eigenvectors[nExt ,j][i][0],extract_eigenvectors[nExt ,j][i][1])
          vectors_init[j,i] = nbre

      final_vector.append(vectors_init)

    return (extract_eigenvalues, final_vector)


def parser_orca_excited2(outputname,N_extract):
    Nstates = N_extract[0]
    N_root = np.delete(N_extract, 0)
    dim_Nroot = len(N_root)

    #TEST VALIDITY OF INPUT 
    sum = 0
    for i in range(0,dim_Nroot):
      sum += N_root[i]

    if sum != Nstates:
      print('##########################')
      print('I hope you know what you are doing, Nstate is suposed to be equal to the sum of the number of root by block wanted!!!')
      print('##########################')
    #####################################

    stringE = 'Eigenvalues:     cm-1         eV      Boltzmann populations at T =  300.000 K'

    # setting indexes to 0
    n_extract_E = 0 #Number of extraction of spin orbit energies

    #dimensioning arrays
    file1 = open(outputname, "r")
    for line in file1: 
      if stringE in line:
        n_extract_E += 1
    file1.close() 

    Energie_line = np.zeros((n_extract_E, Nstates))
    extract_eigenvalues=np.zeros((n_extract_E, Nstates))
    extract_eigenvectors=np.zeros((n_extract_E, Nstates,Nstates,2))

    file1 = open(outputname, "r")
    index = 0
    n_extract_E = 0

    # Filling arrays 
    for line in file1:
        index += 1
        if stringE in line:
          n_extract_E += 1
        
        for istate in range(Nstates):
          if istate < 10 :
            stringV= "STATE   {}:".format(istate)
          else :
            stringV= "STATE  {}:".format(istate)
          if stringV in line:  
            Energie_line[n_extract_E-1,istate] = index
            
            
    for i in range(n_extract_E):
       
      for istate in range(Nstates):
        line_num=int(Energie_line[i,istate])
        extract_eigenvalues[i,istate] = extract_E(outputname, line_num)
        local_vector = extract_vector_component_excited2(outputname,line_num,Nstates,N_root)
        
        for ieigen in range(Nstates):

          extract_eigenvectors[i, istate,ieigen,0]=local_vector[ieigen][0]
          extract_eigenvectors[i, istate,ieigen,1]=local_vector[ieigen][1]

    #Vector changes:
    final_vector = []
    
    for nExt in range(n_extract_E):
      vectors_init = np.zeros((Nstates,Nstates),dtype=complex)
      for i in range(Nstates):
        for j in range(Nstates):
          nbre = complex(extract_eigenvectors[nExt ,j][i][0],extract_eigenvectors[nExt ,j][i][1])
          vectors_init[j,i] = nbre

      final_vector.append(vectors_init)

    return (extract_eigenvalues, final_vector)

def extract_vector_component_excited(fileName, lineNumber ,NVect, N_root):
  #Line number of the vector decomposition in input and return the wanted components
  dim_Nroot = len(N_root)
  with open(fileName,"r") as f:
   data = []
   data = f.readlines()
   local_index = lineNumber #+1
   eigencomponent = []
   i = 0
   Nroot_loc = 0
   
   while i < NVect :
     string_temp = data[local_index]
     string_splited = string_temp.split()
     
     for blockindex in range(0,dim_Nroot):
      if (int(string_splited[4]) == blockindex and (int(string_splited[5]) == 0 or int(string_splited[5]) == 1 or int(string_splited[5]) == 2 or int(string_splited[5]) == 3)  and Nroot_loc < N_root[blockindex] ):
       #print(string_splited)
        eigencomponent.append(extract_real_img_parts(string_temp))
        i += 1
        Nroot_loc += 1
      Nroot_loc = 0
      
     local_index +=1
   return eigencomponent


def extract_vector_component_excited2(fileName, lineNumber ,NVect, N_root):
  #Line number of the vector decomposition in input and return the wanted components
  dim_Nroot = len(N_root)
  with open(fileName,"r") as f:
   data = []
   data = f.readlines()
   local_index = lineNumber #+1
   eigencomponent = []
   i = 0
   Nroot_loc = 0
   
   while i < NVect :
     string_temp = data[local_index]
     string_splited = string_temp.split()
     
     for blockindex in range(0,dim_Nroot):
      if (int(string_splited[4]) == blockindex and (int(string_splited[5]) == 0 or int(string_splited[5]) == 1 or int(string_splited[5]) == 2 or int(string_splited[5]) == 3 or int(string_splited[5]) == 4 or int(string_splited[5]) == 5 or int(string_splited[5]) == 6 or int(string_splited[5]) == 7 or int(string_splited[5]) == 8 or int(string_splited[5]) == 9)  and Nroot_loc < N_root[blockindex] ):
       #print(string_splited)
        eigencomponent.append(extract_real_img_parts(string_temp))
        i += 1
        Nroot_loc += 1
      Nroot_loc = 0
      
     local_index +=1
   return eigencomponent
