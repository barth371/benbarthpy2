import numpy as np
from sympy.physics.quantum.cg import CG
from sympy.physics.quantum.cg import Wigner3j
from sympy import S

#############################################
#  SDS computation for one center
#############################################

#############################################
# Compute S0 = Sz for a given spin S
# return S0 matrix in basis Ms = S , S-1, ... , -S
#############################################
def calc_S0_1center(S) :
    dim = int(2*S+1)
    basis = np.zeros((dim,2))
    S0_matrix = np.zeros((dim, dim))
    for i in range(0,dim):
        basis[i,0] = S
        basis[i,1] = S-i
        S0_matrix[i,i] = basis[i,1]
    return S0_matrix

#############################################
# Compute Sp and Sm for a given spin S
# return Sp matrix in basis Ms = S , S-1, ... , -S
#############################################
def calc_Spm_1center(S) :
    dim = int(2*S+1)
    basis = np.zeros((dim,2))
    Sp_matrix = np.zeros((dim, dim))
    Sm_matrix = np.zeros((dim, dim))
    for i in range(0,dim):
        basis[i,0] = S
        basis[i,1] = S-i

    for i in range(0,dim): # |S,Ms1> state
        Ms1 = basis[i,1]
        Ms_p = Ms1 + 1
        Ms_m = Ms1 - 1
        for j in range(0,dim): #<S,Ms2| state
            if Ms_p == basis[j,1]:
                Sp_matrix[j,i] = np.sqrt(float((S*(S+1)-Ms1*(Ms1+1))/2))
            if Ms_m == basis[j,1]:
                Sm_matrix[j,i] = np.sqrt(float((S*(S+1)-Ms1*(Ms1-1))/2))

    return Sp_matrix, Sm_matrix

def calc_SDS(Dtensor, spin, s0, sp, sm):
    I = complex(0,1)
    dim = int(2*spin+1)
    sz = s0
    sx = (sp+sm)/np.sqrt(2)
    sy = -I*(sp-sm)/np.sqrt(2)
    H_SDS = np.zeros((dim,dim),dtype = complex)
    H_SDS += Dtensor[0, 0] * np.dot(sx, sx)
    H_SDS += Dtensor[1, 1] * np.dot(sy, sy)
    H_SDS += Dtensor[2, 2] * np.dot(sz, sz)
    H_SDS += Dtensor[0, 1] * (np.dot(sx, sy) + np.dot(sy, sx))
    H_SDS += Dtensor[0, 2] * (np.dot(sx, sz) + np.dot(sz, sx))
    H_SDS += Dtensor[1, 2] * (np.dot(sy, sz) + np.dot(sz, sy))
    return H_SDS

def calc_Zeeman_1center(g_tensor,spin,s0,sp,sm,B):
    I = complex(0,1)
    dim = int(2*spin+1)
    Hzeeman=np.zeros((dim,dim),dtype=complex)
    sz = s0
    sx = (sp+sm)/np.sqrt(2)
    sy = -I*(sp-sm)/np.sqrt(2)
    Hzeeman += B[0]*(np.dot(g_tensor[0,0].real,sx) + np.dot(g_tensor[0,1],sy) + np.dot(g_tensor[0,2],sz))
    Hzeeman += B[1] * (np.dot(g_tensor[1,1].real, sy) + np.dot(g_tensor[1,0], sx) + np.dot(g_tensor[1,2], sz))
    Hzeeman += B[2] * (np.dot(g_tensor[2,2].real, sz) + np.dot(g_tensor[2,0], sx) + np.dot(g_tensor[2,1], sy))
    return Hzeeman

#############################################
#  SDS computation for two center
#############################################

#############################################
# Compute the uncoupled basis set for to abitrary spins S1 and S2 and
# return a (N,4) vector, where N is the total numbers of configuration: (2*S1+1)*(2*S2+1)
# and basis[n,0]=S1
#     basis[n,1]=ms1
#     basis[n,2]=S2
#     basis[n,3]=ms2
#Ms1,ms2 deacrease (S , S-1, ... , -S) when n increases
#############################################
def basis_2center(S1,S2):
    dim = int((2*S1+1)*(2*S2+1))
    basis = np.zeros((dim,4))
    cnt = 0
    for i in range(0,int(2*S1+1)):
        for j in range(0,int(2*S2+1)):
            basis[cnt, 0] = S1
            basis[cnt, 1] = S1 - i
            basis[cnt, 2] = S2
            basis[cnt, 3] = S2 - j
            cnt += 1
    return basis

#############################################
# Compute the coupled basis set for to abitrary spins S1 and S2 and
# return a (N,2) vector, where N is the total numbers of configuration: (2*S1+1)*(2*S2+1)
# and basis[n,0]=S
#     basis[n,1]=ms
#############################################
def coupled_basis_2center(S1,S2):
    dim = int((2*S1+1)*(2*S2+1))
    coupled_basis = np.zeros((dim,2))
    #S_max = int(S1+S2)
    S_max = S1+S2
    S_min = abs(S1-S2)
    cnt = 0
    n=int(S_max-S_min+1)
    for i in range(0,n):
        #S_loc=int(S_max-i)
        S_loc=S_max-i
        for j in range(0,int(2*S_loc+1)):
            #print(n,j, S_loc)
            coupled_basis[cnt, 0] = S_loc
            coupled_basis[cnt, 1] = S_loc - j
            cnt += 1
    return coupled_basis

def Wigner3J_uncoupled_to_coupled(basis_uncoupled,basis_coupled):
    dim = np.shape(basis_uncoupled)[0]
    # coupled_basis = np.zeros((dim,2))
    U_basis = np.zeros((dim, dim))
    for i in range(0, dim):  # |S3,Ms3> state
        for j in range(0, dim):  # <s1,ms1,s2,ms2| state
            # I use the same notation as the librairy : https://docs.sympy.org/latest/modules/physics/quantum/cg.html
            s1 = basis_uncoupled[j, 0]
            ms1 = basis_uncoupled[j, 1]
            s2 = basis_uncoupled[j, 2]
            ms2 = basis_uncoupled[j, 3]
            # Coupled basis
            S3 = basis_coupled[i, 0]
            MS3 = basis_coupled[i, 1]

            U_basis[j, i] = np.sqrt(2*float(S3)+1) * Wigner3j(s1, ms1, s2, ms2, S3, -MS3).doit()

    return U_basis

def Clebsch_Gordan_uncoupled_to_coupled(basis_uncoupled,basis_coupled):
    dim = np.shape(basis_uncoupled)[0]
    #coupled_basis = np.zeros((dim,2))
    U_basis = np.zeros((dim,dim))
    for i in range(0,dim): # |S3,Ms3> state
        for j in range(0,dim): #<s1,ms1,s2,ms2| state
            #I use the same notation as the librairy : https://docs.sympy.org/latest/modules/physics/quantum/cg.html
            s1 = basis_uncoupled[j,0]
            ms1= basis_uncoupled[j,1]
            s2= basis_uncoupled[j,2]
            ms2= basis_uncoupled[j,3]
            #Coupled basis
            S3= basis_coupled[i,0]
            MS3= basis_coupled[i,1]

            U_basis[j,i]= CG(s1, ms1, s2, ms2, S3 ,MS3).doit()

    return U_basis


def calc_S0_2center(basis):
    dim = np.shape(basis)[0]
    s0a = np.zeros((dim,dim))
    s0b = np.zeros((dim,dim))
    for i in range(0,dim):
        s0a[i,i] = basis[i,1]
        s0b[i,i] = basis[i,3]
    #print('dim = ',dim)
    return s0a, s0b

def calc_Spm_2center(basis):
    dim = np.shape(basis)[0]

    spa = np.zeros((dim,dim))
    spb = np.zeros((dim,dim))
    sma = np.zeros((dim, dim))
    smb = np.zeros((dim, dim))

    for i in range(0,dim): # |S,Ms1> state
        Sa = basis[i,0]
        Sb = basis[i,2]
        Msa = basis[i,1]
        Msb = basis[i,3]
        Msap = Msa + 1
        Msam = Msa - 1
        Msbp = Msb + 1
        Msbm = Msb - 1
        for j in range(0,dim): #<S,Ms2| state
            if Msap == basis[j,1] and Msb == basis[j,3]:
               #spa[j,i] = np.sqrt(float((Sa*(Sa+1)-Msa*(Msa+1))/2))
                spa[j,i] = -np.sqrt(float((Sa*(Sa+1)-Msa*(Msa+1))/2))
            if Msam == basis[j,1] and Msb == basis[j,3]:
                sma[j,i] = np.sqrt(float((Sa*(Sa+1)-Msa*(Msa-1))/2))
            if Msbp == basis[j,3] and Msa == basis[j,1]:
               #spb[j,i] = np.sqrt(float((Sb*(Sb+1)-Msb*(Msb+1))/2))
                spb[j,i] = -np.sqrt(float((Sb*(Sb+1)-Msb*(Msb+1))/2))
            if Msbm == basis[j,3] and Msa == basis[j,1]:
                smb[j,i] = np.sqrt(float((Sb*(Sb+1)-Msb*(Msb-1))/2))
    return spa, spb, sma, smb


def calc_JSaSb(J,basis):
    dim = np.shape(basis)[0]
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    H_heisenberg = np.zeros((dim,dim))
    H_heisenberg = J * (np.dot(s0a,s0b)-np.dot(spa,smb)-np.dot(sma,spb))
    return H_heisenberg

def calc_SaDSb(Dab_tensor, basis):
    dim = np.shape(basis)[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    sza = s0a
    szb = s0b
   #sxa = (spa+sma)/np.sqrt(2)
   #sya = -I*(spa-sma)/np.sqrt(2)
   #sxb = (spb+smb)/np.sqrt(2)
   #syb = -I*(spb-smb)/np.sqrt(2)
    sxa = -(spa - sma) / np.sqrt(2)
    sya = I * (spa + sma) / np.sqrt(2)
    sxb = -(spb - smb) / np.sqrt(2)
    syb = I * (spb + smb) / np.sqrt(2)
    H_ex_aniso = np.zeros((dim,dim),dtype = complex)
    H_ex_aniso += Dab_tensor[0, 0] * np.dot(sxa, sxb)
    H_ex_aniso += Dab_tensor[1, 1] * np.dot(sya, syb)
    H_ex_aniso += Dab_tensor[2, 2] * np.dot(sza, szb)
    H_ex_aniso += Dab_tensor[0, 1] * (np.dot(sxa, syb) + np.dot(sya, sxb))
    H_ex_aniso += Dab_tensor[0, 2] * (np.dot(sxa, szb) + np.dot(sza, sxb))
    H_ex_aniso += Dab_tensor[1, 2] * (np.dot(sya, szb) + np.dot(sza, syb))
    return H_ex_aniso

def calc_SaDaSa_SbDbSb(Da_tensor, Db_tensor, basis):
    dim = np.shape(basis)[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    sza = s0a
    szb = s0b
   #sxa = (spa+sma)/np.sqrt(2)
   #sya = -I*(spa-sma)/np.sqrt(2)
   #sxb = (spb+smb)/np.sqrt(2)
   #syb = -I*(spb-smb)/np.sqrt(2)
    sxa = -(spa - sma) / np.sqrt(2)
    sya = I * (spa + sma) / np.sqrt(2)
    sxb = -(spb - smb) / np.sqrt(2)
    syb = I * (spb + smb) / np.sqrt(2)
    H_SaDaSa = np.zeros((dim,dim),dtype = complex)
    H_SbDbSb = np.zeros((dim,dim),dtype = complex)

    H_SaDaSa += Da_tensor[0, 0] * np.dot(sxa, sxa)
    H_SaDaSa += Da_tensor[1, 1] * np.dot(sya, sya)
    H_SaDaSa += Da_tensor[2, 2] * np.dot(sza, sza)
    H_SaDaSa += Da_tensor[0, 1] * (np.dot(sxa, sya) + np.dot(sya, sxa))
    H_SaDaSa += Da_tensor[0, 2] * (np.dot(sxa, sza) + np.dot(sza, sxa))
    H_SaDaSa += Da_tensor[1, 2] * (np.dot(sya, sza) + np.dot(sza, sya))

    H_SbDbSb += Db_tensor[0, 0] * np.dot(sxb, sxb)
    H_SbDbSb += Db_tensor[1, 1] * np.dot(syb, syb)
    H_SbDbSb += Db_tensor[2, 2] * np.dot(szb, szb)
    H_SbDbSb += Db_tensor[0, 1] * (np.dot(sxb, syb) + np.dot(syb, sxb))
    H_SbDbSb += Db_tensor[0, 2] * (np.dot(sxb, szb) + np.dot(szb, sxb))
    H_SbDbSb += Db_tensor[1, 2] * (np.dot(syb, szb) + np.dot(szb, syb))

    return H_SaDaSa, H_SbDbSb

def calc_dab_SaxSb(dab_vector, basis):
    dim = np.shape(basis)[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    sza = s0a
    szb = s0b
   #sxa = (spa+sma)/np.sqrt(2)
   #sya = -I*(spa-sma)/np.sqrt(2)
   #sxb = (spb+smb)/np.sqrt(2)
   #syb = -I*(spb-smb)/np.sqrt(2)
    sxa = -(spa - sma) / np.sqrt(2)
    sya = I * (spa + sma) / np.sqrt(2)
    sxb = -(spb - smb) / np.sqrt(2)
    syb = I * (spb + smb) / np.sqrt(2)

    H_DMI = np.zeros((dim,dim),dtype = complex)
    H_DMI += dab_vector[2] * (np.dot(sxa, syb) - np.dot(sya, sxb))
    H_DMI -= dab_vector[1] * (np.dot(sxa, szb) - np.dot(sza, sxb))
    H_DMI += dab_vector[0] * (np.dot(sya, szb) - np.dot(sza, syb))
    return H_DMI

def calc_rank4_Dab(D4_tensor, basis):
    dim = np.shape(basis)[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    sza = s0a
    szb = s0b
    sxa = (spa+sma)/np.sqrt(2)
    sya = -I*(spa-sma)/np.sqrt(2)
    sxb = (spb+smb)/np.sqrt(2)
    syb = -I*(spb-smb)/np.sqrt(2)
    sa = [sxa,sya,sza]
    sb = [sxb,syb,szb]

    H_rank4 = np.zeros((dim,dim),dtype = complex)
    for i in range(0,3):
        for j in range(0,3):
            saxsa = np.dot(sa[i],sa[j])
            for k in range(0,3):
                for l in range(0,3):
                    sbxsb = np.dot(sb[k],sb[l])
                    H_rank4 += D4_tensor[i,j,k,l]*np.dot(saxsa,sbxsb)

    return H_rank4

def calc_localrank4_DA(D4_tensor, basis):
    dim = np.shape(basis)[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    sza = s0a
    sxa = (spa+sma)/np.sqrt(2)
    sya = -I*(spa-sma)/np.sqrt(2)
    sa = [sxa,sya,sza]

    HA_local_rank4 = np.zeros((dim,dim),dtype = complex)
    for i in range(0,3):
        for j in range(0,3):
            saxsa = np.dot(sa[i],sa[j])
            for k in range(0,3):
                for l in range(0,3):
                    sbxsb = np.dot(sa[k],sa[l])
                    HA_local_rank4 += D4_tensor[i,j,k,l]*np.dot(saxsa,sbxsb)

    return HA_local_rank4

#test rank 6
def calc_rank6_Dab(D6_tensor, basis):
    dim = np.shape(basis)[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    sza = s0a
    szb = s0b
    sxa = (spa+sma)/np.sqrt(2)
    sya = -I*(spa-sma)/np.sqrt(2)
    sxb = (spb+smb)/np.sqrt(2)
    syb = -I*(spb-smb)/np.sqrt(2)
    sa = [sxa,sya,sza]
    sb = [sxb,syb,szb]

    H_rank6 = np.zeros((dim,dim),dtype = complex)
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                saxsaxsa = np.dot(np.dot(sa[i],sa[j]),sa[k])
                for l in range(0,3):
                    for m in range(0,3):
                        for n in range(0,3):
                            sbxsbxsb = np.dot(np.dot(sb[l],sb[m]),sb[n])
                            H_rank6 += D6_tensor[i,j,k,l,m,n]*np.dot(saxsaxsa,sbxsbxsb)

    return H_rank6

# Test rank 3 : Sa*Sa.Dab.Sb
def calc_rank3_Dab(D3_tensor_left, basis):
    dim = np.shape(basis)[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    sza = s0a
    szb = s0b
    sxa = (spa+sma)/np.sqrt(2)
    sya = -I*(spa-sma)/np.sqrt(2)
    sxb = (spb+smb)/np.sqrt(2)
    syb = -I*(spb-smb)/np.sqrt(2)
    sa = [sxa,sya,sza]
    sb = [sxb,syb,szb]

    H_rank3 = np.zeros((dim,dim),dtype = complex)
    for i in range(0,3):
        for j in range(0,3):
            saxsa = np.dot(sb[i],sb[j])
            for k in range(0,3):
                H_rank3 += D3_tensor_left[i,j,k]*np.dot(saxsa,sa[k])

    return H_rank3

###########################################################
# build test hamiltonian with only local D, Jx, Jy,Jz
###########################################################
def H1_test(basis, DxA,DzA,DxB,DzB,Jx,Jy,Jz) :
    dim = np.shape(basis)[0]
    H1 = np.zeros((dim,dim),dtype=complex)
    Dab_tensor = np.zeros((3,3))
    Da_tensor = np.zeros((3, 3))
    Db_tensor = np.zeros((3, 3))

    Da_tensor[0,0] = DxA
    Da_tensor[1,1] = DxA
    Da_tensor[2,2] = DzA

    Db_tensor[0,0] = DxB
    Db_tensor[1,1] = DxB
    Db_tensor[2,2] = DzB

    Dab_tensor[0,0] = Jx
    Dab_tensor[1,1] = Jx
    Dab_tensor[2,2] = Jz

    H1 += calc_SaDSb(Dab_tensor,basis)
    Ha_local, Hb_local = calc_SaDaSa_SbDbSb(Da_tensor, Db_tensor, basis)
    # print('Ha_local')
    # print(Ha_local)
    # print('Hb_local')
    # print(Hb_local)
    H1 += Ha_local + Hb_local
    return H1

###########################################################
# build test hamiltonian with only local D (diagonal)
###########################################################
def H2_test(basis, Dx, Dy, Dz) :
    dim = np.shape(basis)[0]
    spin = basis[0,0]
    D_tensor = np.zeros((3,3))
    D_tensor[0,0] = Dx
    D_tensor[1,1] = Dy
    D_tensor[2,2] = Dz
    s0 = calc_S0_1center(spin)
    sp,sm= calc_Spm_1center(spin)
    H2 = calc_SDS(D_tensor, spin, s0, sp, sm)
    return H2

###########################################################
# build full Hamiltonian in uncoupled basis
###########################################################
def H_2center(basis, Da_tensor, Db_tensor, Dab_tensor, dab_vector):
    dim = np.shape(basis)[0]
    H_local_A, H_local_B = calc_SaDaSa_SbDbSb(Da_tensor,Db_tensor,basis)
    H_symmetric_AB = calc_SaDSb(Dab_tensor,basis)
    H_antisymetric_AB = calc_dab_SaxSb(dab_vector,basis)
    H = H_local_A + H_local_B + H_symmetric_AB + H_antisymetric_AB
    return H

###########################################################
# build full Hamiltonian (with rank 4) in uncoupled basis
###########################################################
def H_2center_rank4(basis, Da_tensor, Db_tensor, Dab_tensor, dab_vector,DAB_rank4_tensor):
    dim = np.shape(basis)[0]
    H_local_A, H_local_B = calc_SaDaSa_SbDbSb(Da_tensor,Db_tensor,basis)
    H_symmetric_AB = calc_SaDSb(Dab_tensor,basis)
    H_antisymetric_AB = calc_dab_SaxSb(dab_vector,basis)
    H_rank4 = calc_rank4_Dab(DAB_rank4_tensor,basis)
    H = H_local_A + H_local_B + H_symmetric_AB + H_antisymetric_AB + H_rank4
    return H
