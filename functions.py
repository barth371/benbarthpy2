import numpy as np
from h_models import *
#import matplotlib as mpl
#import matplotlib.pyplot as plt
import math
#from scipy.linalg.lapack import zheev
#from scipy.linalg.blas import zgemm
#from scipy.linalg.blas import zdotc
#import scipy as sp
np.set_printoptions(precision=3,suppress=True)

#################################################
# Construct the effective Hamiltonian from initial
# vectors and energies
# input : vectors (N*N matrix, vectors in row)
#        energies (N vector)
# output : Heff (N*N matrix)
#################################################
def heff_build(vectors,energies):
    np.set_printoptions(precision=8, suppress=True)
    dim_H = len(energies)

    # overlap matrix between projected vectors
    S_matrix = np.zeros((dim_H,dim_H),dtype=complex)
    for i in range(0,dim_H):
        vec1 = vectors[i,:].copy()
        for j in range(0,dim_H):
            vec2 = vectors[j, :].copy()
            S_matrix[i,j] = np.dot(np.conj(vec2),vec1)
   #print('Overlap :')
   #print(S_matrix)

    # inversion of the overlap matrix
    #S_matrix_inverse = np.linalg.inv(S_matrix)
    #print('Inversion of overlap matrix :')
    #print(S_matrix_inverse)

    # check if inversion is OK, should return identity
    #print('S-1.S')
    #print(np.dot(S_matrix,S_matrix_inverse))

    # diagonalize S
    S_eigenval, S_eigenvec = np.linalg.eigh(S_matrix) #vectors in column
    # print('S matrix eigenvalues')
    # print(S_eigenval)

    # define transfer matrix P = S_eigenvec
    P = S_eigenvec
    # print('P matrix')
    # print(P)

    # invert the transfer matrix Pm1 = (P)-1
    Pm1 = np.linalg.inv(P)
    # print('Pm1 matrix')
    # print(Pm1)

    # check if Pm1*S*P = Sdiag
    # Sdiag = np.dot(Pm1,np.dot(S_matrix,P))
   #print('Sdiag :')
   #print(Sdiag)
   #print(S_eigenval)

    # compute Sm12 diagonal : diagonal matrix with element 1/sqrt(sdiag(i,i)
    Sm12_diag = np.zeros((dim_H,dim_H))
    for i in range(0,dim_H):
        Sm12_diag[i,i]=1/np.sqrt(S_eigenval[i])
    # print('Sm12_diag:')
    # print(Sm12_diag)

    # transform Sm12 in the initial basis Sm12 = P.Sm12_diag.Pm1
    Sm12 = np.dot(P,np.dot(Sm12_diag,Pm1))
    # print('Sm12:')
    # print(Sm12)

    # compute (Sm12)*|P> and conjugate
    vec = np.dot(Sm12,vectors)
    vec_conj =np.conj(vec)
    # print('vec')
    # print(vec)
    # print('vec_conj')
    # print(vec_conj)

    # compute Heff = Sum|Sm12*Pk>*Ek*<Sm12*Pk|
    heff = np.zeros((dim_H, dim_H),dtype=complex)
    for i in range(0,dim_H): # loop over <S,MSi|
        for j in range(0,dim_H): # loop over |S,MSj>
            for k in range(0,dim_H): # loop over k
                heff[i,j] += vec[k,i] * energies[k] * vec_conj[k,j]

    return heff

##################################
####Orthonormalization methods####
##################################

def Gram_schmidt(A):
   #np.set_printoptions(precision=15)
    res = A.copy()
    for i in range(res.shape[1]):
        for j in range(i):
            res[:,i] = res[:,i] - np.dot(res[:,j],res[:,i])*res[:,j]
        res[:,i]=res[:,i]/np.linalg.norm(res[:,i])
    return res

####Bonus Gram-Schimdt working for colinear vectors!!!
def Gram_schmidt2(X):
    O = np.zeros(X.shape)
    for i in range(X.shape[1]):
        # orthogonalization
        vector = X[:, i]
        space = O[:, :i]
        projection = vector @ space
        vector = vector - np.sum(projection * space, axis=1)

        # normalization
        norm = np.sqrt(vector @ vector)
        vector /= abs(norm) < 1e-8 and 1 or norm
        
        O[:, i] = vector    
    return O

def Des_cloizeaux(vectors):
    #np.set_printoptions(precision=12, suppress=True)
    np.set_printoptions(precision=15)
    dim_H = len(vectors[0,:])
   #print(dim_H)
   #print(vectors)
    # overlap matrix between projected vectors
    S_matrix = np.zeros((dim_H,dim_H),dtype=float)
    for i in range(0,dim_H):
        vec1 = vectors[:,i].copy()
        for j in range(0,dim_H):
            vec2 = vectors[:, j].copy()
            S_matrix[i,j] = np.dot(np.conj(vec2),vec1)
   #print('Overlap :')
   #print(S_matrix)

    # diagonalize S
    S_eigenval, S_eigenvec = np.linalg.eigh(S_matrix) #vectors in column
   #print('S matrix eigenvalues')
   #print(S_eigenval)
    # define transfer matrix P = S_eigenvec
    P = S_eigenvec
    # print('P matrix')
    # print(P)

    # invert the transfer matrix Pm1 = (P)-1
    Pm1 = np.linalg.inv(P)
    # print('Pm1 matrix')
    # print(Pm1)

    # check if Pm1*S*P = Sdiag
    # Sdiag = np.dot(Pm1,np.dot(S_matrix,P))
   #print('Sdiag :')
    #print(Sdiag)
   #print(S_eigenval)

    # compute Sm12 diagonal : diagonal matrix with element 1/sqrt(sdiag(i,i)
    Sm12_diag = np.zeros((dim_H,dim_H))
    for i in range(0,dim_H):
        Sm12_diag[i,i]=1/np.sqrt(S_eigenval[i])
   #print('Sm12_diag:')
   #print(Sm12_diag)

    # transform Sm12 in the initial basis Sm12 = P.Sm12_diag.Pm1
    Sm12 = np.dot(P,np.dot(Sm12_diag,Pm1))
   #print('Sm12:')
   #print(Sm12)

    # compute (Sm12)*|P> and conjugate
    #vec = np.dot(Sm12,vectors)
    vec = np.transpose(np.dot(Sm12,np.transpose(vectors)))
    vec_conj =np.conj(vec)
   #print('vec')
   #print(vec)
    # print('vec_conj')
    # print(vec_conj)

    return vec

def solver_1center(heff,spin):

    label = ['x','y','z']
    n_parameters = 6
    list_parameters = np.zeros((n_parameters),dtype=object)
    list_tensors = np.zeros((n_parameters,3,3))

    i_cnt = 0
    for i_param in range(0,3):
        for j_param in range(i_param,3):
            D_tensor = np.zeros((3,3))
            D_tensor[i_param,j_param] = 1
            D_tensor[j_param,i_param] = 1
            list_tensors[i_cnt] = D_tensor
            list_parameters[i_cnt]= 'D' + label[i_param]+label[j_param]
            i_cnt +=1
    print(list_tensors)
    print(list_parameters)

    dim_heff = np.shape(heff)[0]
    s0 = calc_S0_1center(spin)
    sp,sm = calc_Spm_1center(spin)

    A = np.zeros((2*dim_heff**2,n_parameters))
    b = np.zeros((2*dim_heff**2))

    #build the b vector
    i_cnt = 0
    for i in range(0,dim_heff):
        for j in range(0,dim_heff):
            b[i_cnt] = heff[i,j].real
            b[i_cnt+dim_heff**2] = heff[i,j].imag
            i_cnt += 1
    print('b vector = ')
    print(b)

    #build the A matrix
    i_cnt = 0
    for i_param in range(0,n_parameters):
        H_model = calc_SDS(list_tensors[i_param],spin,s0,sp,sm)
        # print('H_model')
        # print(H_model)
        H_model_real = H_model.real
        H_model_imag = H_model.imag
        # print('H_model_real')
        # print(H_model_real)
        # print('H_model_imag')
        # print(H_model_imag)

        i_cnt2 = 0
        for i in range(0,dim_heff):
            for j in range(0,dim_heff):
                A[i_cnt2,i_cnt] = H_model_real[i,j]
                A[i_cnt2+dim_heff**2,i_cnt] = H_model_imag[i,j]
                i_cnt2 += 1

        i_cnt += 1
    print('A matrix = ')
    print(A)

    Ainv = np.linalg.pinv(A)
    print('A pseudo-inverse = ')
    print(Ainv)

    # compute the p vector
    P = np.dot(Ainv, b)
    P_round = np.around(P,8)

    print('Solution = ')
    for i in range(0,n_parameters):
        print(list_parameters[i], ' = ' , P_round[i])

    return

# solver for DA, DB, DAB and dAB
def solver_2center(heff,S1,S2):
    n_parameters = 21
    dim_heff = np.shape(heff)[0]
    basis = basis_2center(S1, S2)

    # build the list of tensor for each parameter
    list_tensor_DA = np.zeros((n_parameters,3,3))
    list_tensor_DB = np.zeros((n_parameters,3,3))
    list_tensor_DAB = np.zeros((n_parameters,3,3))
    list_vector_dAB = np.zeros((n_parameters,3))
    i_cnt = 0
    for i in range(0,3):
        for j in range(i,3):
            D = np.zeros((3, 3))
            D[i,j] = 1
            D[j,i] = 1
            list_tensor_DA[i_cnt] = D
            list_tensor_DB[i_cnt+6] = D
            list_tensor_DAB[i_cnt+12] = D
            i_cnt +=1
    list_vector_dAB[18,0] = 1
    list_vector_dAB[19,1] = 1
    list_vector_dAB[20,2] = 1

    #build the b vector
    b = np.zeros((2 * dim_heff ** 2))
    i_cnt = 0
    for i in range(0,dim_heff):
        for j in range(0,dim_heff):
            b[i_cnt] = heff[i,j].real
            b[i_cnt+dim_heff**2] = heff[i,j].imag
            i_cnt += 1
    #print('b vector = ')
    #print(b)

    #build the A matrix
    A = np.zeros((2 * dim_heff ** 2, n_parameters))
    i_cnt = 0
    for i_param in range(0,n_parameters):
        H_model = H_2center(basis, list_tensor_DA[i_param], list_tensor_DB[i_param],
                            list_tensor_DAB[i_param], list_vector_dAB[i_param])
        H_model_real = H_model.real
        H_model_imag = H_model.imag

        i_cnt2 = 0
        for i in range(0,dim_heff):
            for j in range(0,dim_heff):
                A[i_cnt2,i_cnt] = H_model_real[i,j]
                A[i_cnt2+dim_heff**2,i_cnt] = H_model_imag[i,j]
                i_cnt2 += 1

        i_cnt += 1
    #print('A matrix = ')
    #print(A)

    Ainv = np.linalg.pinv(A)
    #print('A pseudo-inverse = ')
    #print(Ainv)

    # compute the p vector
    P = np.dot(Ainv, b)
    #P= np.around(P,8)

    DA = np.array([[P[0],P[1],P[2]],[P[1],P[3],P[4]],[P[2],P[4],P[5]]])
    DB = np.array([[P[6],P[7],P[8]],[P[7],P[9],P[10]],[P[8],P[10],P[11]]])
    DAB = np.array([[P[12],P[13],P[14]],[P[13],P[15],P[16]],[P[14],P[16],P[17]]])
    dAB = np.array([P[18],P[19],P[20]])

    return DA,DB,DAB,dAB

# solver for DA, DB, DAB, dAB and DAB_rank4
def solver_2center_rank4(heff,S1,S2):
    n_parameters = 21+81
    dim_heff = np.shape(heff)[0]
    basis = basis_2center(S1, S2)

    # build the list of tensor for each parameter
    list_tensor_DA = np.zeros((n_parameters,3,3))
    list_tensor_DB = np.zeros((n_parameters,3,3))
    list_tensor_DAB = np.zeros((n_parameters,3,3))
    list_vector_dAB = np.zeros((n_parameters,3))
    list_tensor_DAB_rank4 = np.zeros((n_parameters,3,3,3,3))
    i_cnt = 0
    for i in range(0,3):
        for j in range(i,3):
            D = np.zeros((3, 3))
            D[i,j] = 1
            D[j,i] = 1
            list_tensor_DA[i_cnt] = D
            list_tensor_DB[i_cnt+6] = D
            list_tensor_DAB[i_cnt+12] = D
            i_cnt +=1
    list_vector_dAB[18,0] = 1
    list_vector_dAB[19,1] = 1
    list_vector_dAB[20,2] = 1
    cnt = 21
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                for l in range(0,3):
                    DAB_rank4 = np.zeros((3, 3,3,3))
                    DAB_rank4[i,j,k,l] = 1
                    list_tensor_DAB_rank4[cnt] = DAB_rank4
                    cnt+=1

    #build the b vector
    b = np.zeros((2 * dim_heff ** 2))
    i_cnt = 0
    for i in range(0,dim_heff):
        for j in range(0,dim_heff):
            b[i_cnt] = heff[i,j].real
            b[i_cnt+dim_heff**2] = heff[i,j].imag
            i_cnt += 1
    #print('b vector = ')
    #print(b)

    #build the A matrix
    A = np.zeros((2 * dim_heff ** 2, n_parameters))
    i_cnt = 0
    for i_param in range(0,n_parameters):
        H_model = H_2center_rank4(basis, list_tensor_DA[i_param], list_tensor_DB[i_param],
                            list_tensor_DAB[i_param], list_vector_dAB[i_param], list_tensor_DAB_rank4[i_param])
        H_model_real = H_model.real
        H_model_imag = H_model.imag

        i_cnt2 = 0
        for i in range(0,dim_heff):
            for j in range(0,dim_heff):
                A[i_cnt2,i_cnt] = H_model_real[i,j]
                A[i_cnt2+dim_heff**2,i_cnt] = H_model_imag[i,j]
                i_cnt2 += 1

        i_cnt += 1
    #print('A matrix = ')
    #print(A)

    Ainv = np.linalg.pinv(A)
    #print('A pseudo-inverse = ')
    #print(Ainv)

    # compute the p vector
    P = np.dot(Ainv, b)
    P= np.around(P,8)

    # print('Solution = ')
    # print(P)

    DA = np.array([[P[0],P[1],P[2]],[P[1],P[3],P[4]],[P[2],P[4],P[5]]])
    DB = np.array([[P[6],P[7],P[8]],[P[7],P[9],P[10]],[P[8],P[10],P[11]]])
    DAB = np.array([[P[12],P[13],P[14]],[P[13],P[15],P[16]],[P[14],P[16],P[17]]])
    dAB = np.array([P[18],P[19],P[20]])
    cnt = 21
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                for l in range(0,3):
                    DAB_rank4[i,j,k,l] = P[cnt]
                    cnt += 1

    return DA,DB,DAB,dAB,DAB_rank4

# solver for DA, DB, DAB, dAB and DAB_rank4
def solver_2center_local_rank4(heff,S1,S2):
    n_parameters = 21+81
    dim_heff = np.shape(heff)[0]
    basis = basis_2center(S1, S2)

    # build the list of tensor for each parameter
    list_tensor_DA = np.zeros((n_parameters,3,3))
    list_tensor_DB = np.zeros((n_parameters,3,3))
    list_tensor_DAB = np.zeros((n_parameters,3,3))
    list_vector_dAB = np.zeros((n_parameters,3))
    list_tensor_DA_rank4 = np.zeros((n_parameters,3,3,3,3))
    i_cnt = 0
    for i in range(0,3):
        for j in range(i,3):
            D = np.zeros((3, 3))
            D[i,j] = 1
            D[j,i] = 1
            list_tensor_DA[i_cnt] = D
            list_tensor_DB[i_cnt+6] = D
            list_tensor_DAB[i_cnt+12] = D
            i_cnt +=1
    list_vector_dAB[18,0] = 1
    list_vector_dAB[19,1] = 1
    list_vector_dAB[20,2] = 1
    cnt = 21
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                for l in range(0,3):
                    DA_rank4 = np.zeros((3, 3,3,3))
                    DA_rank4[i,j,k,l] = 1
                    list_tensor_DA_rank4[cnt] = DA_rank4
                    cnt+=1

    #build the b vector
    b = np.zeros((2 * dim_heff ** 2))
    i_cnt = 0
    for i in range(0,dim_heff):
        for j in range(0,dim_heff):
            b[i_cnt] = heff[i,j].real
            b[i_cnt+dim_heff**2] = heff[i,j].imag
            i_cnt += 1
    #print('b vector = ')
    #print(b)

    #build the A matrix
    A = np.zeros((2 * dim_heff ** 2, n_parameters))
    i_cnt = 0
    for i_param in range(0,n_parameters):
        H_model = H_2center_localrank4(basis, list_tensor_DA[i_param], list_tensor_DB[i_param],
                            list_tensor_DAB[i_param], list_vector_dAB[i_param], list_tensor_DA_rank4[i_param])
        H_model_real = H_model.real
        H_model_imag = H_model.imag

        i_cnt2 = 0
        for i in range(0,dim_heff):
            for j in range(0,dim_heff):
                A[i_cnt2,i_cnt] = H_model_real[i,j]
                A[i_cnt2+dim_heff**2,i_cnt] = H_model_imag[i,j]
                i_cnt2 += 1

        i_cnt += 1
    #print('A matrix = ')
    #print(A)

    Ainv = np.linalg.pinv(A)
    #print('A pseudo-inverse = ')
    #print(Ainv)

    # compute the p vector
    P = np.dot(Ainv, b)
    P= np.around(P,8)

    # print('Solution = ')
    # print(P)

    DA = np.array([[P[0],P[1],P[2]],[P[1],P[3],P[4]],[P[2],P[4],P[5]]])
    DB = np.array([[P[6],P[7],P[8]],[P[7],P[9],P[10]],[P[8],P[10],P[11]]])
    DAB = np.array([[P[12],P[13],P[14]],[P[13],P[15],P[16]],[P[14],P[16],P[17]]])
    dAB = np.array([P[18],P[19],P[20]])
    cnt = 21
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                for l in range(0,3):
                    DA_rank4[i,j,k,l] = P[cnt]
                    cnt += 1

    return DA,DB,DAB,dAB,DA_rank4

# solver for DA, DB, DAB, dAB and DAB_rank3
def solver_2center_rank3(heff,S1,S2):
    n_parameters = 21+27
    dim_heff = np.shape(heff)[0]
    basis = basis_2center(S1, S2)

    # build the list of tensor for each parameter
    list_tensor_DA = np.zeros((n_parameters,3,3))
    list_tensor_DB = np.zeros((n_parameters,3,3))
    list_tensor_DAB = np.zeros((n_parameters,3,3))
    list_vector_dAB = np.zeros((n_parameters,3))
    list_tensor_DAB_rank3 = np.zeros((n_parameters,3,3,3))
    i_cnt = 0
    for i in range(0,3):
        for j in range(i,3):
            D = np.zeros((3, 3))
            D[i,j] = 1
            D[j,i] = 1
            list_tensor_DA[i_cnt] = D
            list_tensor_DB[i_cnt+6] = D
            list_tensor_DAB[i_cnt+12] = D
            i_cnt +=1
    list_vector_dAB[18,0] = 1
    list_vector_dAB[19,1] = 1
    list_vector_dAB[20,2] = 1
    cnt = 21
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                DAB_rank3 = np.zeros((3, 3,3))
                DAB_rank3[i,j,k] = 1
                list_tensor_DAB_rank3[cnt] = DAB_rank3
                cnt+=1

    #build the b vector
    b = np.zeros((2 * dim_heff ** 2))
    i_cnt = 0
    for i in range(0,dim_heff):
        for j in range(0,dim_heff):
            b[i_cnt] = heff[i,j].real
            b[i_cnt+dim_heff**2] = heff[i,j].imag
            i_cnt += 1
    #print('b vector = ')
    #print(b)

    #build the A matrix
    A = np.zeros((2 * dim_heff ** 2, n_parameters))
    i_cnt = 0
    for i_param in range(0,n_parameters):
        H_model = H_2center_rank3(basis, list_tensor_DA[i_param], list_tensor_DB[i_param],
                            list_tensor_DAB[i_param], list_vector_dAB[i_param], list_tensor_DAB_rank3[i_param])
        H_model_real = H_model.real
        H_model_imag = H_model.imag

        i_cnt2 = 0
        for i in range(0,dim_heff):
            for j in range(0,dim_heff):
                A[i_cnt2,i_cnt] = H_model_real[i,j]
                A[i_cnt2+dim_heff**2,i_cnt] = H_model_imag[i,j]
                i_cnt2 += 1

        i_cnt += 1
    #print('A matrix = ')
    #print(A)

    Ainv = np.linalg.pinv(A)
    #print('A pseudo-inverse = ')
    #print(Ainv)

    # compute the p vector
    P = np.dot(Ainv, b)
    P= np.around(P,8)

    # print('Solution = ')
    # print(P)

    DA = np.array([[P[0],P[1],P[2]],[P[1],P[3],P[4]],[P[2],P[4],P[5]]])
    DB = np.array([[P[6],P[7],P[8]],[P[7],P[9],P[10]],[P[8],P[10],P[11]]])
    DAB = np.array([[P[12],P[13],P[14]],[P[13],P[15],P[16]],[P[14],P[16],P[17]]])
    dAB = np.array([P[18],P[19],P[20]])
    cnt = 21
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                DAB_rank3[i,j,k] = P[cnt]
                cnt += 1

    return DA,DB,DAB,dAB,DAB_rank3

# solver for DA, DB, DAB, dAB and DAB_rank6
def solver_2center_rank6(heff,S1,S2):
    n_parameters = 21+729
    dim_heff = np.shape(heff)[0]
    basis = basis_2center(S1, S2)

    # build the list of tensor for each parameter
    list_tensor_DA = np.zeros((n_parameters,3,3))
    list_tensor_DB = np.zeros((n_parameters,3,3))
    list_tensor_DAB = np.zeros((n_parameters,3,3))
    list_vector_dAB = np.zeros((n_parameters,3))
    list_tensor_DAB_rank6 = np.zeros((n_parameters,3,3,3,3,3,3))
    i_cnt = 0
    for i in range(0,3):
        for j in range(i,3):
            D = np.zeros((3, 3))
            D[i,j] = 1
            D[j,i] = 1
            list_tensor_DA[i_cnt] = D
            list_tensor_DB[i_cnt+6] = D
            list_tensor_DAB[i_cnt+12] = D
            i_cnt +=1
    list_vector_dAB[18,0] = 1
    list_vector_dAB[19,1] = 1
    list_vector_dAB[20,2] = 1
    cnt = 21
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                for l in range(0, 3):
                    for m in range(0, 3):
                        for n in range(0, 3):
                            DAB_rank6 = np.zeros((3,3,3,3,3,3))
                            DAB_rank6[i,j,k,l,m,n] = 1
                            list_tensor_DAB_rank6[cnt] = DAB_rank6
                            cnt+=1

    #build the b vector
    b = np.zeros((2 * dim_heff ** 2))
    i_cnt = 0
    for i in range(0,dim_heff):
        for j in range(0,dim_heff):
            b[i_cnt] = heff[i,j].real
            b[i_cnt+dim_heff**2] = heff[i,j].imag
            i_cnt += 1
    #print('b vector = ')
    #print(b)

    #build the A matrix
    A = np.zeros((2 * dim_heff ** 2, n_parameters))
    i_cnt = 0
    for i_param in range(0,n_parameters):
        H_model = H_2center_rank6(basis, list_tensor_DA[i_param], list_tensor_DB[i_param],
                            list_tensor_DAB[i_param], list_vector_dAB[i_param], list_tensor_DAB_rank6[i_param])
        H_model_real = H_model.real
        H_model_imag = H_model.imag

        i_cnt2 = 0
        for i in range(0,dim_heff):
            for j in range(0,dim_heff):
                A[i_cnt2,i_cnt] = H_model_real[i,j]
                A[i_cnt2+dim_heff**2,i_cnt] = H_model_imag[i,j]
                i_cnt2 += 1

        i_cnt += 1
    #print('A matrix = ')
    #print(A)

    Ainv = np.linalg.pinv(A)
    #print('A pseudo-inverse = ')
    #print(Ainv)

    # compute the p vector
    P = np.dot(Ainv, b)
    P= np.around(P,8)

    # print('Solution = ')
    # print(P)

    DA = np.array([[P[0],P[1],P[2]],[P[1],P[3],P[4]],[P[2],P[4],P[5]]])
    DB = np.array([[P[6],P[7],P[8]],[P[7],P[9],P[10]],[P[8],P[10],P[11]]])
    DAB = np.array([[P[12],P[13],P[14]],[P[13],P[15],P[16]],[P[14],P[16],P[17]]])
    dAB = np.array([P[18],P[19],P[20]])
    cnt = 21
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                for l in range(0, 3):
                    for m in range(0, 3):
                        for n in range(0, 3):
                            DAB_rank6[i,j,k,l,m,n] = P[cnt]
                            cnt += 1

    return DA,DB,DAB,dAB,DAB_rank6

def error_model(heff,h_model):
    diffH = heff-h_model
    mae = np.mean(abs(diffH))
    maxae = np.max(abs(diffH))
    rmse = np.sqrt(np.mean(diffH**2))
    return mae, maxae, rmse
