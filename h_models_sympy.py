from sympy import *
from sympy.physics.quantum.cg import CG
import sys


#********UNCOUPLED PART*********
def basis_2center(S1,S2):
    dim = int((2*S1+1)*(2*S2+1))
   #basis = np.zeros((dim,4))
    basis = zeros(dim,4) 
    cnt = 0
    for i in range(0,int(2*S1+1)):
        for j in range(0,int(2*S2+1)):
            basis[cnt, 0] = S1
            basis[cnt, 1] = S1 - i
            basis[cnt, 2] = S2
            basis[cnt, 3] = S2 - j
            cnt += 1
    return basis

def calc_S0_2center(basis):
    dim = basis.shape[0]
    s0a = zeros(dim,dim)
    s0b = zeros(dim,dim)
    for i in range(0,dim):
        s0a[i,i] = basis[i,1]
        s0b[i,i] = basis[i,3]
    #print('dim = ',dim)
    return s0a, s0b

def calc_Spm_2center(basis):
    dim = basis.shape[0]

    spa = zeros(dim,dim)
    spb = zeros(dim,dim)
    sma = zeros(dim,dim)
    smb = zeros(dim,dim)

    for i in range(0,dim): # |S,Ms1> state
        Sa = basis[i,0]
        Sb = basis[i,2]
        Msa = basis[i,1]
        Msb = basis[i,3]
        Msap = Msa + 1
        Msam = Msa - 1
        Msbp = Msb + 1
        Msbm = Msb - 1
        for j in range(0,dim): #<S,Ms2| state
            if Msap == basis[j,1] and Msb == basis[j,3]:
               #spa[j,i] = -np.sqrt(float((Sa*(Sa+1)-Msa*(Msa+1))/2))
               #spa[j,i] = -sqrt(float((Sa*(Sa+1)-Msa*(Msa+1))/2))
                spa[j,i] = nsimplify(sqrt((Sa*(Sa+1)-Msa*(Msa+1))))
            if Msam == basis[j,1] and Msb == basis[j,3]:
               #sma[j,i] = np.sqrt(float((Sa*(Sa+1)-Msa*(Msa-1))/2))
               #sma[j,i] = sqrt(float((Sa*(Sa+1)-Msa*(Msa-1))/2))
                sma[j,i] = nsimplify(sqrt((Sa*(Sa+1)-Msa*(Msa-1))))
            if Msbp == basis[j,3] and Msa == basis[j,1]:
               #spb[j,i] = -np.sqrt(float((Sb*(Sb+1)-Msb*(Msb+1))/2))
               #spb[j,i] = -sqrt(float((Sb*(Sb+1)-Msb*(Msb+1))/2))
                spb[j,i] = nsimplify(sqrt((Sb*(Sb+1)-Msb*(Msb+1))))
            if Msbm == basis[j,3] and Msa == basis[j,1]:
               #smb[j,i] = np.sqrt(float((Sb*(Sb+1)-Msb*(Msb-1))/2))
               #smb[j,i] = sqrt(float((Sb*(Sb+1)-Msb*(Msb-1))/2))
               #pprint(nsimplify((Sb*(Sb+1)-Msb*(Msb-1))/2))
                smb[j,i] = nsimplify(sqrt((Sb*(Sb+1)-Msb*(Msb-1))))
               #smb[j,i] = (Sb*(Sb+1)-Msb*(Msb-1))/2
    return spa, spb, sma, smb


def calc_JSaSb(J,basis):
    dim = basis.shape[0]
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    H_heisenberg = zeros(dim,dim)
   #H_heisenberg = J * (np.dot(s0a,s0b)-np.dot(spa,smb)-np.dot(sma,spb))
   #H_heisenberg = nsimplify(J * (s0a*s0b-spa*smb-sma*spb))
    H_heisenberg = nsimplify(J * (s0a*s0b+(spa*smb+sma*spb)/2))
    return H_heisenberg


def calc_SaDSb(Dab_tensor, basis):
    dim = basis.shape[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)
    sza = nsimplify(s0a)
    szb = nsimplify(s0b)
    sxa = nsimplify( (spa + sma) / 2 )
    sya = nsimplify( -I * (spa - sma) /2)
    sxb = nsimplify( (spb + smb) / 2)
    syb = nsimplify( -I * (spb - smb) / 2)
    H_ex_aniso = zeros(dim,dim)
    H_ex_aniso +=nsimplify( Dab_tensor[0, 0] * (sxa * sxb))
    H_ex_aniso +=nsimplify( Dab_tensor[1, 1] * (sya * syb))
    H_ex_aniso +=nsimplify( Dab_tensor[2, 2] * (sza * szb))
    H_ex_aniso +=nsimplify( Dab_tensor[0, 1] * ((sxa * syb) + (sya * sxb)))
    H_ex_aniso +=nsimplify( Dab_tensor[0, 2] * ((sxa * szb) + (sza * sxb)))
    H_ex_aniso +=nsimplify( Dab_tensor[1, 2] * ((sya * szb) + (sza * syb)))
    return H_ex_aniso


def calc_dab_SaxSb(dab_vector, basis):
    dim = basis.shape[0]
    I = complex(0, 1)
    s0a,s0b=calc_S0_2center(basis)
    spa, spb, sma, smb = calc_Spm_2center(basis)

    sza = nsimplify(s0a)
    szb = nsimplify(s0b)
    sxa = nsimplify( (spa + sma) / 2 )
    sya = nsimplify( -I * (spa - sma) /2)
    sxb = nsimplify( (spb + smb) / 2)
    syb = nsimplify( -I * (spb - smb) / 2)

    H_DMI = zeros(dim,dim)
    H_DMI += nsimplify(dab_vector[2] * ((sxa *  syb) - (sya *  sxb)))
    H_DMI -= nsimplify(dab_vector[1] * ((sxa *  szb) - (sza *  sxb)))
    H_DMI += nsimplify(dab_vector[0] * ((sya *  szb) - (sza *  syb)))
    return H_DMI


###############COUPLED PART##################
def coupled_basis_2center(S1,S2):
    dim = int((2*S1+1)*(2*S2+1))
    coupled_basis = zeros(dim,2)
    #S_max = int(S1+S2)
    S_max = S1+S2
    S_min = abs(S1-S2)
    cnt = 0
    n=int(S_max-S_min+1)
    for i in range(0,n):
        #S_loc=int(S_max-i)
        S_loc=S_max-i
        for j in range(0,int(2*S_loc+1)):
            #print(n,j, S_loc)
            coupled_basis[cnt, 0] = S_loc
            coupled_basis[cnt, 1] = S_loc - j
            cnt += 1
    return coupled_basis


def Clebsch_Gordan_uncoupled_to_coupled(basis_uncoupled,basis_coupled):
    dim = basis_uncoupled.shape[0]
    #coupled_basis = np.zeros((dim,2))
    U_basis = zeros(dim,dim)
    for i in range(0,dim): # |S3,Ms3> state
        for j in range(0,dim): #<s1,ms1,s2,ms2| state
            #I use the same notation as the librairy : https://docs.sympy.org/latest/modules/physics/quantum/cg.html
            s1 = basis_uncoupled[j,0]
            ms1= basis_uncoupled[j,1]
            s2= basis_uncoupled[j,2]
            ms2= basis_uncoupled[j,3]
            #Coupled basis
            S3= basis_coupled[i,0]
            MS3= basis_coupled[i,1]

            U_basis[j,i]=nsimplify(CG(s1, ms1, s2, ms2, S3 ,MS3).doit())

    return U_basis
