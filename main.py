from parser import *
from functions import *
from h_models import *
#from plot import *
import numpy as np
import sys
np.set_printoptions(precision=1,linewidth=300,suppress=True)

###################################################################
# Input section
###################################################################
# The orca output to analyse
#utput = "Results/Cu2Cl5_d0.02_y001_verif"
#output = "cuace_geom_mieux/cu2acetate.y01"
#output = "CAS64_6t10s/Cu2Cl5.theta180phi150_0.01"
#output = "CAS64_6t10s/Cu2Cl5.theta180phi125_0.000"
output = "locPasloc/Cu2Cl5.CAS.E.out"
output = "Bart_dirty_messy_stuff/CompMolcasOrca/OrcaNoField"
output = "Cu2Cl5_QDNEVP2/QDNEVPT2TEST/Cu2Cl5.3T3S_NEVPT2"
#output = "locPasloc/Cu2Cl5.CAS.E.loc.purif2.out"
#output = "locPasloc/selonZ/Cu2Cl5.CAS.E.out"
#output = "locPasloc/selonZ/Cu2Cl5.CAS.E.loc.purif2.out"
#output = "locPasloc/selonX/Cu2Cl5.CAS.E.out"
#output = "femn_mrci.out"
#output = "locPasloc/selonX/Cu2Cl5.CAS.E.loc.purif2.out"
Nextract_orca = 0 # if multiple SOC calculations, select the one to use (start at 0)
#spin = 1 # first spin
S2 = 1/2# second spin
S1 = 1/2# second spin
dim_Heff = int((2*S1+1)*(2*S2+1))
list1 = [dim_Heff,3,1]

###################################################################
# Parsing the Orca output
###################################################################
print('Parsing the output file              ... ',end='')
N_extract = np.array(list1)
energies, vectors = parser_orca(output,N_extract)
print('done')
print('   Number of extractions : ',len(energies))

###################################################################
# Building the effective Hamiltonian
###################################################################
for i in range(0,1):
  i = len(energies) - 1
  print('Initial vectors :')
  print(vectors[i])
  print('Initial energies : ')
  print(energies[i])
  print('            ',end='')
  print('Building the effective Hamiltonian  extracxt nulber ',i ,end='')
  heff = heff_build(vectors[i],energies[i])
  print('done')
  print('Effective Hamiltonian :')
  print(heff)
  print('vectors[i]')
  print(vectors[i])
  print('energies[i]')
  print(energies[i])
  S_eigenval, S_eigenvec = np.linalg.eigh(heff)
  print("Heff vector")
  print(S_eigenvec)  
  print("Heff E")
  print(S_eigenval)  
#solver_1center(heff,spin)
#DA,DB,DAB,dAB = solver_2center(heff_uncoupled,S1,S2) # without rank 4 tensor

#sys.exit()

###################################################################
# Building the basis and matrices to go from coupled to uncoupled
###################################################################
print('Expressing H in uncoupled basis      ... ',end='')
basis_uncoupled = basis_2center(S1,S2)
basis_coupled = coupled_basis_2center(S1,S2)
print(basis_uncoupled,basis_coupled)
U = Clebsch_Gordan_uncoupled_to_coupled(basis_uncoupled,basis_coupled)
print(U)
Uinv = np.linalg.inv(U)
heff_uncoupled = np.dot(U,np.dot(heff,Uinv)) # express heff in uncoupled basis
print('done')
print('Effective Hamiltonian in coupled basis :')
print(heff)
print('Effective Hamiltonian in uncoupled basis :')
print(heff_uncoupled)

###################################################################
# Solving the least-square problem to get SH parameters
###################################################################
print('Extracting SH parameters             ... ',end='')
DA,DB,DAB,dAB = solver_2center(heff_uncoupled,S1,S2) # without rank 4 tensor
#DA,DB,DAB,dAB,DAB_rank4 = solver_2center_rank4(heff_uncoupled,S1,S2) # with rank 4 tensor
print('done')

###################################################################
# Building the model H and comparing with effective H
###################################################################
print('Computing errors                     ... ',end='')
H = H_2center(basis_uncoupled,DA,DB,DAB,dAB) # without rank 4 tensor
#H = H_2center_rank4(basis_uncoupled,DA,DB,DAB,dAB,DAB_rank4) # with rank 4 tensor
Herror_uncoupled = H-heff_uncoupled
Herror_coupled = np.dot(Uinv,np.dot(Herror_uncoupled,U))
mae, maxae, rmse = error_model(heff_uncoupled,H)
eigenvalue_model = np.linalg.eigvalsh(H)
mae_energy = np.mean(abs(eigenvalue_model-energies[i]))
print('done')
print('   MAE(H)  = ', mae)
print('   MaxE(H) = ', maxae)
print('   MAE(E)  = ', mae_energy)

###################################################################
# Printing results
###################################################################
print('\n========================')
print('  Extracted Parameters')
print('========================\n')

DA_trace = np.trace(DA)
DA = DA - np.dot(DA_trace/3,np.identity((3)))
print('DA (Trace = ',DA_trace,')')
print(DA)
print('')

DB_trace = np.trace(DB)
DB = DB - np.dot(DB_trace/3,np.identity((3)))
print('DB (Trace = ',DB_trace,')')
print(DB)
print('')

DAB_trace = np.trace(DAB)
DAB = DAB - np.dot(DAB_trace/3,np.identity((3)))
print('J isotropic = ', DAB_trace/3,'\n')
print('DAB (Trace = ',DAB_trace,')')
print(DAB)
print('')

eigenvalue_DAV = np.linalg.eigvalsh(DAB)

print('DAB Eigvalues = ',eigenvalue_DAV)

print('')
print('dAB')
print(dAB)
print('')

# test plot eigenvectors
#plot_axis(DA,DB,DAB,dAB)

print('Final line')
print('J Dx Dy Dz dx dy dz')
print(DAB_trace/3, eigenvalue_DAV[0],eigenvalue_DAV[1],eigenvalue_DAV[2],dAB[0],dAB[1],dAB[2])
sys.exit()

print(mae)
print(maxae)
print(mae_energy)
print(DA_trace)
print(DB_trace)
print(DAB_trace)
for i in range(0,3):
    for j in range(i,3):
        print(DA[i,j])
for i in range(0,3):
    for j in range(i,3):
        print(DB[i,j])
for i in range(0,3):
    for j in range(i,3):
        print(DAB[i,j])
for i in range(0,3):
    print(dAB[i])
#or i in range(0, 3):
#   for j in range(0, 3):
#       for k in range(0, 3):
#           for l in range(0, 3):
#               print(DAB_rank4[i,j,k,l])
